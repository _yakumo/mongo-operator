all: push

IMAGE_NAME ?= cargo.caicloud.io/sysinfra/mongo-operator
IMAGE_TAG ?= $(shell cat VERSION)

local: main.go
	CGO_ENABLED=0 go build -a -installsuffix cgo -ldflags '-w' -o operator .

operator: main.go
	CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -ldflags '-w' -o operator .

container: operator
	docker build -t $(IMAGE_NAME):$(IMAGE_TAG) .

push: container
	docker push $(IMAGE_NAME):$(IMAGE_TAG)

latest: operator
	docker build -t $(IMAGE_NAME):latest .
	docker push $(IMAGE_NAME):latest

e2e: operator
	docker build -t $(IMAGE_NAME):e2e .
	docker push $(IMAGE_NAME):e2e

clean:
	rm -f operator
