# mongo operator

**Project status: *alpha*** Not all planned feature are completed. The API, spec, status and other user facing objects are subject to change. We do not support backward-compatibility for the alpha releases.

Mongo operator manages mongo clusters atop [Kubernetes](http://kubernetes.io/), automating their creation and administration:

* [Create and destroy mongo cluster](#create-and-destroy-mongo-cluster)
* [Recover a member](#member-recovery)
* [Scale out mongo sharding cluster](#scale-out-sharding-cluster)


## Requirements
* Kubernetes 1.4+
* mongodb 3.4+

## Build
Mongo operator can be compiled with Make

|target|description
|---|---|
|all, push| build operator and build docker image then push |
|local| build operator locally, according to your system arch |
|operator| build operator with GOOS=linux |
|container| build docker image |
|latest | build docker image with latest tag then push |
|e2e| build docker image with e2e tag then push, for testing|
|clean| remove binary |

## Test
First run make e2e to build image from latest source, then change to e2e folder and use go test or ginkgo to run e2e tests. It is recommended to run e2e test in k8s cluster to avoid network issues. Note, make e2e will push a new image tagged with 'e2e' to specified docker registry, make sure this is desirable.

```
$ make e2e
$ cd e2e
$ IN_CLUSTER=true ginkgo -v
$ IN_CLUSTER=true go test
```

## Deploy mongo operator
```
$ kubectl create -f example/deployment.yaml
deployment "mongo-operator" created
```

mongo operator will create a Kubernetes Third-Party Resource (TPR) "Mongo" automatically.

```
$ kubectl get thirdpartyresources
NAME                      DESCRIPTION                                 VERSION(S)
mongo.infra.caicloud.io   A new resource representing Mongo Cluster   v1
```

## Create and destroy mongo cluster
```
$ kubectl create -f example/example-mongo-replset-with-arbiter.yaml
```

A mongo cluster with 2 replicas and 1 arbiter will be created:

```
$ kubectl get pods
NAME                   READY     STATUS    RESTARTS   AGE
example-mongo-bd3m1x   1/1       Running   0          1m
example-mongo-oiciet   1/1       Running   0          1m
example-mongo-w4zs5e   1/1       Running   0          1m

$ kubectl get services
NAME                   CLUSTER-IP       EXTERNAL-IP   PORT(S)     AGE
example-mongo          10.254.102.208   <none>        27017/TCP   1m
example-mongo-bd3m1x   None             <none>        27017/TCP   1m
example-mongo-oiciet   None             <none>        27017/TCP   1m
example-mongo-w4zs5e   None             <none>        27017/TCP   1m

$ kubectl get mongos example -o yaml
apiVersion: infra.caicloud.io/v1
detailed:
  csrs: {}
  mongos: {}
kind: Mongo
metadata:
  creationTimestamp: 2017-01-12T06:30:42Z
  name: example
  namespace: example
  resourceVersion: "7663920"
  selfLink: /apis/infra.caicloud.io/v1/namespaces/example/mongos/example
  uid: a45c1faa-d890-11e6-8a7e-0800274a55b6
spec:
  image: mongo
  replicas: 2
  version: "3.4"
status:
  primary: example-mongo-bd3m1x
  state: RUNNING

$ mongo example-mongo
example-mongo:PRIMARY> rs.status()
{
	"set" : "example-mongo",
	"date" : ISODate("2017-01-12T06:33:01.591Z"),
	"myState" : 1,
	...
	"members" : [
		{
			"_id" : 0,
			"name" : "example-mongo-bd3m1x:27017",
			"health" : 1,
			"state" : 1,
			"stateStr" : "PRIMARY",
			...
			"self" : true
		},
		{
			"_id" : 1,
			"name" : "example-mongo-oiciet:27017",
			"health" : 1,
			"state" : 2,
			"stateStr" : "SECONDARY",
			...
			"syncingTo" : "example-mongo-bd3m1x:27017",
			"configVersion" : 1
		},
		{
			"_id" : 2,
			"name" : "example-mongo-w4zs5e:27017",
			"health" : 1,
			"state" : 7,
			"stateStr" : "ARBITER",
			...
		}
	],
	"ok" : 1
}


```

Destroy cluster

```
$ kubectl delete -f example/example-mongo-replset-with-arbiter.yaml
```

---

```
$ kubectl create -f example/example-mongo-replset-no-arbiter.yaml
```
A cluster with 3 replicas will be created:

```
$ kubectl get pods
NAME                   READY     STATUS    RESTARTS   AGE
example-mongo-7mul3t   1/1       Running   0          33s
example-mongo-e1xt48   1/1       Running   0          43s
example-mongo-shasu0   1/1       Running   0          23s

$ kubectl get services
NAME                   CLUSTER-IP      EXTERNAL-IP   PORT(S)     AGE
example-mongo          10.254.32.158   <none>        27017/TCP   24s
example-mongo-7mul3t   None            <none>        27017/TCP   1m
example-mongo-e1xt48   None            <none>        27017/TCP   1m
example-mongo-shasu0   None            <none>        27017/TCP   50s

$ kubectl get mongos example -o yaml
apiVersion: infra.caicloud.io/v1
detailed:
  csrs: {}
  mongos: {}
kind: Mongo
metadata:
  creationTimestamp: 2017-01-12T06:34:45Z
  name: example
  namespace: example
  resourceVersion: "7664596"
  selfLink: /apis/infra.caicloud.io/v1/namespaces/example/mongos/example
  uid: 352cf5bc-d891-11e6-8c5f-0800274a55b6
spec:
  image: mongo
  replicas: 3
  version: "3.4"
status:
  primary: example-mongo-e1xt48
  state: RUNNING
  
$ mongo example-mongo
example-mongo:PRIMARY> rs.status()
{
	"set" : "example-mongo",
	"date" : ISODate("2017-01-12T06:36:55.020Z"),
	"myState" : 1,
	...
	"members" : [
		{
			"_id" : 0,
			"name" : "example-mongo-e1xt48:27017",
			"health" : 1,
			"state" : 1,
			"stateStr" : "PRIMARY",
			...
			"self" : true
		},
		{
			"_id" : 1,
			"name" : "example-mongo-7mul3t:27017",
			"health" : 1,
			"state" : 2,
			"stateStr" : "SECONDARY",
			...
			"syncingTo" : "example-mongo-e1xt48:27017",
			"configVersion" : 1
		},
		{
			"_id" : 2,
			"name" : "example-mongo-shasu0:27017",
			"health" : 1,
			"state" : 2,
			"stateStr" : "SECONDARY",
			...
			"syncingTo" : "example-mongo-7mul3t:27017",
			"configVersion" : 1
		}
	],
	"ok" : 1
}
```

Destroy cluster

```
$ kubectl delete -f example/example-mongo-replset-no-arbiter.yaml
```

---

```
$ kubectl create -f example/example-mongo-sharding-cluster.yaml 
```

A mongo sharding cluster will be created, with 2 shards and a configsvr replica set

```
$ kubectl get pods
NAME                             READY     STATUS    RESTARTS   AGE
example-configsvr-mongo-cm5eaj   1/1       Running   0          1m
example-configsvr-mongo-r4jbf9   1/1       Running   0          1m
example-configsvr-mongo-xiieli   1/1       Running   0          1m
example-mongos-p101jf            1/1       Running   0          47s
example-mongos-wwmr8i            1/1       Running   0          37s
example-shard0-mongo-7t8nha      1/1       Running   0          32s
example-shard0-mongo-azhy9h      1/1       Running   0          22s
example-shard0-mongo-d8b4er      1/1       Running   0          12s
example-shard1-mongo-75zges      1/1       Running   0          32s
example-shard1-mongo-aod0hh      1/1       Running   0          22s
example-shard1-mongo-ionfnm      1/1       Running   0          12s

$ kubectl get services
NAME                             CLUSTER-IP       EXTERNAL-IP   PORT(S)     AGE
example-configsvr-mongo          10.254.55.113    <none>        27017/TCP   1m
example-configsvr-mongo-cm5eaj   None             <none>        27017/TCP   1m
example-configsvr-mongo-r4jbf9   None             <none>        27017/TCP   1m
example-configsvr-mongo-xiieli   None             <none>        27017/TCP   1m
example-mongos                   10.254.33.50     <none>        27017/TCP   1m
example-shard0-mongo             10.254.236.121   <none>        27017/TCP   2s
example-shard0-mongo-7t8nha      None             <none>        27017/TCP   49s
example-shard0-mongo-azhy9h      None             <none>        27017/TCP   39s
example-shard0-mongo-d8b4er      None             <none>        27017/TCP   28s
example-shard1-mongo             10.254.191.88    <none>        27017/TCP   8s
example-shard1-mongo-75zges      None             <none>        27017/TCP   49s
example-shard1-mongo-aod0hh      None             <none>        27017/TCP   39s
example-shard1-mongo-ionfnm      None             <none>        27017/TCP   28s

$ kubectl get mongos example -o yaml
apiVersion: infra.caicloud.io/v1
detailed:
  csrs:
    primary: example-configsvr-mongo-xiieli
    state: RUNNING
  mongos:
    hosts:
    - 192.168.68.17
    - 192.168.72.12
    state: RUNNING
  shards:
    example-shard0:
      primary: example-shard0-mongo-7t8nha
      state: RUNNING
    example-shard1:
      primary: example-shard1-mongo-75zges
      state: RUNNING
kind: Mongo
metadata:
  creationTimestamp: 2017-01-12T06:39:24Z
  name: example
  namespace: example
  resourceVersion: "7665617"
  selfLink: /apis/infra.caicloud.io/v1/namespaces/example/mongos/example
  uid: db8621a0-d891-11e6-8a7e-0800274a55b6
spec:
  image: mongo
  sharding:
    collections:
    - collection: user
      db: test
      hashed: false
      key: _id
      unique: false
    shards: 2
  version: "3.4"
status:
  state: RUNNING
  
$ mongo example-mongos
mongos> sh.status()
--- Sharding Status --- 
  sharding version: {
	"_id" : 1,
	"minCompatibleVersion" : 5,
	"currentVersion" : 6,
	"clusterId" : ObjectId("587724c5570ae18202ae780c")
}
  shards:
	{  "_id" : "example-shard0-mongo",  "host" : "example-shard0-mongo/example-shard0-mongo-7t8nha:27017,example-shard0-mongo-azhy9h:27017",  "state" : 1 }
	{  "_id" : "example-shard1-mongo",  "host" : "example-shard1-mongo/example-shard1-mongo-75zges:27017,example-shard1-mongo-aod0hh:27017",  "state" : 1 }
  databases:
	{  "_id" : "test",  "primary" : "example-shard0-mongo",  "partitioned" : true }
		test.user
			shard key: { "_id" : 1 }
			chunks:
				example-shard0-mongo	1
			{ "_id" : { "$minKey" : 1 } } -->> { "_id" : { "$maxKey" : 1 } } on : example-shard0-mongo Timestamp(1, 0) 
```

Destroy cluster

```
$ kubectl delete -f example/example-mongo-sharding-cluster.yaml
```

## Member recovery
```
$ kubectl create -f example/example-mongo-replset-no-arbiter.yaml

$ kubectl get po
NAME                   READY     STATUS    RESTARTS   AGE
example-mongo-03xta6   1/1       Running   0          48s
example-mongo-7b4mzi   1/1       Running   0          58s
example-mongo-z5cact   1/1       Running   0          38s

$ kubectl get mongos example -o yaml
apiVersion: infra.caicloud.io/v1
detailed:
  csrs: {}
  mongos: {}
kind: Mongo
metadata:
  creationTimestamp: 2017-01-12T06:46:35Z
  name: example
  namespace: example
  resourceVersion: "7666709"
  selfLink: /apis/infra.caicloud.io/v1/namespaces/example/mongos/example
  uid: dc3b54c8-d892-11e6-8a7e-0800274a55b6
spec:
  image: mongo
  replicas: 3
  version: "3.4"
status:
  primary: example-mongo-7b4mzi
  state: RUNNING
```

```
$ kubectl delete po example-mongo-7b4mzi

// wait for some seconds

$ kubectl get po
NAME                   READY     STATUS              RESTARTS   AGE
example-mongo-03xta6   1/1       Running             0          4m
example-mongo-7b4mzi   0/1       ContainerCreating   0          4s
example-mongo-z5cact   1/1       Running             0          3m

$ kubectl get mongos example -o yaml
apiVersion: infra.caicloud.io/v1
detailed:
  csrs: {}
  mongos: {}
kind: Mongo
metadata:
  creationTimestamp: 2017-01-12T06:46:35Z
  name: example
  namespace: example
  resourceVersion: "7666968"
  selfLink: /apis/infra.caicloud.io/v1/namespaces/example/mongos/example
  uid: dc3b54c8-d892-11e6-8a7e-0800274a55b6
spec:
  image: mongo
  replicas: 3
  version: "3.4"
status:
  primary: example-mongo-03xta6
  state: RECOVERING
```

```
// wait for recovery

$ kubectl get po
NAME                   READY     STATUS    RESTARTS   AGE
example-mongo-03xta6   1/1       Running   0          4m
example-mongo-7b4mzi   1/1       Running   0          46s
example-mongo-z5cact   1/1       Running   0          4m

$ kubectl get mongos example -o yaml
apiVersion: infra.caicloud.io/v1
detailed:
  csrs: {}
  mongos: {}
kind: Mongo
metadata:
  creationTimestamp: 2017-01-12T06:46:35Z
  name: example
  namespace: example
  resourceVersion: "7667018"
  selfLink: /apis/infra.caicloud.io/v1/namespaces/example/mongos/example
  uid: dc3b54c8-d892-11e6-8a7e-0800274a55b6
spec:
  image: mongo
  replicas: 3
  version: "3.4"
status:
  primary: example-mongo-03xta6
  state: RUNNING
```

## Scale out sharding cluster
```
$ kubectl create -f example/example-mongo-sharding-cluster.yaml 
$ kubectl get po
NAME                             READY     STATUS    RESTARTS   AGE
example-configsvr-mongo-5jsd55   1/1       Running   0          1m
example-configsvr-mongo-yakktd   1/1       Running   0          1m
example-configsvr-mongo-yvdnq2   1/1       Running   0          1m
example-mongos-ax6200            1/1       Running   0          54s
example-mongos-x9ckf3            1/1       Running   0          1m
example-shard0-mongo-533zws      1/1       Running   0          39s
example-shard0-mongo-dzociu      1/1       Running   0          49s
example-shard0-mongo-rognkl      1/1       Running   0          28s
example-shard1-mongo-muzt94      1/1       Running   0          28s
example-shard1-mongo-oujbhf      1/1       Running   0          39s
example-shard1-mongo-xxntqs      1/1       Running   0          49s

$ kubectl get mongos example -o yaml
apiVersion: infra.caicloud.io/v1
detailed:
  csrs:
    primary: example-configsvr-mongo-yvdnq2
    state: RUNNING
  mongos:
    hosts:
    - 192.168.68.17
    - 192.168.72.12
    state: RUNNING
  shards:
    example-shard0:
      primary: example-shard0-mongo-dzociu
      state: RUNNING
    example-shard1:
      primary: example-shard1-mongo-xxntqs
      state: RUNNING
kind: Mongo
metadata:
  creationTimestamp: 2017-01-12T06:54:56Z
  name: example
  namespace: example
  resourceVersion: "7668337"
  selfLink: /apis/infra.caicloud.io/v1/namespaces/example/mongos/example
  uid: 071bcf89-d894-11e6-8a7e-0800274a55b6
spec:
  image: mongo
  sharding:
    collections:
    - collection: user
      db: test
      hashed: false
      key: _id
      unique: false
    shards: 2
  version: "3.4"
status:
  state: RUNNING
  
// use vim to edit example/example-mongo-sharding-cluster.yaml, change spec.sharding.shards to 3
$ kubectl replace -f example/example-mongo-sharding-cluster.yaml

$ kubectl get mongos example -o yaml
apiVersion: infra.caicloud.io/v1
detailed:
  csrs:
    primary: example-configsvr-mongo-yvdnq2
    state: RUNNING
  mongos:
    hosts:
    - 192.168.68.17
    - 192.168.72.12
    state: RUNNING
  shards:
    example-shard0:
      primary: example-shard0-mongo-dzociu
      state: RUNNING
    example-shard1:
      primary: example-shard1-mongo-xxntqs
      state: RUNNING
kind: Mongo
metadata:
  creationTimestamp: 2017-01-12T06:54:56Z
  name: example
  namespace: example
  resourceVersion: "7668728"
  selfLink: /apis/infra.caicloud.io/v1/namespaces/example/mongos/example
  uid: 071bcf89-d894-11e6-8a7e-0800274a55b6
spec:
  image: mongo
  sharding:
    collections:
    - collection: user
      db: test
      hashed: false
      key: _id
      unique: false
    shards: 3
  version: "3.4"
status:
  state: SCALING

$ kubectl get po
NAME                             READY     STATUS    RESTARTS   AGE
example-configsvr-mongo-5jsd55   1/1       Running   0          5m
example-configsvr-mongo-yakktd   1/1       Running   0          4m
example-configsvr-mongo-yvdnq2   1/1       Running   0          5m
example-mongos-ax6200            1/1       Running   0          4m
example-mongos-x9ckf3            1/1       Running   0          4m
example-shard0-mongo-533zws      1/1       Running   0          4m
example-shard0-mongo-dzociu      1/1       Running   0          4m
example-shard0-mongo-rognkl      1/1       Running   0          3m
example-shard1-mongo-muzt94      1/1       Running   0          3m
example-shard1-mongo-oujbhf      1/1       Running   0          4m
example-shard1-mongo-xxntqs      1/1       Running   0          4m
example-shard2-mongo-9vge2r      1/1       Running   0          37s
example-shard2-mongo-rjuwep      1/1       Running   0          17s
example-shard2-mongo-zso1p1      1/1       Running   0          27s

// new shards added
$ mongo example-mongos
mongos> sh.status()
--- Sharding Status --- 
  sharding version: {
	"_id" : 1,
	"minCompatibleVersion" : 5,
	"currentVersion" : 6,
	"clusterId" : ObjectId("5877286d38f6436d680c830f")
}
  shards:
	{  "_id" : "example-shard0-mongo",  "host" : "example-shard0-mongo/example-shard0-mongo-533zws:27017,example-shard0-mongo-dzociu:27017",  "state" : 1 }
	{  "_id" : "example-shard1-mongo",  "host" : "example-shard1-mongo/example-shard1-mongo-oujbhf:27017,example-shard1-mongo-xxntqs:27017",  "state" : 1 }
	{  "_id" : "example-shard2-mongo",  "host" : "example-shard2-mongo/example-shard2-mongo-9vge2r:27017,example-shard2-mongo-zso1p1:27017",  "state" : 1 }
  databases:
	{  "_id" : "test",  "primary" : "example-shard0-mongo",  "partitioned" : true }
		test.user
			shard key: { "_id" : 1 }
			chunks:
				example-shard0-mongo	1
			{ "_id" : { "$minKey" : 1 } } -->> { "_id" : { "$maxKey" : 1 } } on : example-shard0-mongo Timestamp(1, 0) 
```

## Planned Feature
* Anti-affinity of pods
* Dump data to file for backup
* Upgrade mongo version ?
* Upgrade a mongo replica set to a sharding cluster ?
