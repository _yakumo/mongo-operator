package e2e

import (
	"fmt"
	"os"
	"strconv"
	"time"

	"k8s.io/client-go/1.5/pkg/api"
	apierrors "k8s.io/client-go/1.5/pkg/api/errors"
	"k8s.io/client-go/1.5/pkg/api/v1"

	"github.com/caicloud/operators/mongodb/pkg"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func createOperatorPod(client *pkg.K8sClient, name string) {
	pod := &v1.Pod{
		ObjectMeta: v1.ObjectMeta{
			Name:      name,
			Namespace: "kube-system",
		},
		Spec: v1.PodSpec{
			Containers: []v1.Container{
				{
					Name:            "operator",
					Image:           "cargo.caicloud.io/sysinfra/mongo-operator:e2e",
					ImagePullPolicy: v1.PullAlways,
					Args:            []string{"--running-in-cluster=true", "--logtostderr=1"},
				},
			},
			TerminationGracePeriodSeconds: func() *int64 { i := int64(300); return &i }(),
		},
	}
	_, err := client.Core().Pods("kube-system").Create(pod)
	Expect(err).To(BeNil())
}

func deleteOperatorPod(client *pkg.K8sClient, name string) {
	err := client.Core().Pods("kube-system").Delete(name, &api.DeleteOptions{})
	Expect(err).To(BeNil())
}

func waitForPodRunning(client *pkg.K8sClient, ns, name string, timeout time.Duration) error {
	now := time.Now()
	deadline := now.Add(timeout)
	for {
		pod, err := client.Core().Pods(ns).Get(name)
		if time.Now().After(deadline) {
			return fmt.Errorf("timeout")
		}
		if err != nil {
			return err
		}
		if pod.Status.Phase != v1.PodRunning {
			time.Sleep(time.Second)
			continue
		}
		break
	}
	return nil
}

func waitForPodToDisappear(client *pkg.K8sClient, ns, name string, timeout time.Duration) error {
	now := time.Now()
	deadline := now.Add(timeout)
	for {
		_, err := client.Core().Pods(ns).Get(name)
		if time.Now().After(deadline) {
			return fmt.Errorf("timeout")
		}
		if apierrors.IsNotFound(err) {
			break
		}
		if err != nil {
			return err
		}
		time.Sleep(time.Second)
	}
	return nil
}

func waitForTPRToDisappear(client *pkg.K8sClient, timeout time.Duration) error {
	now := time.Now()
	deadline := now.Add(timeout)
	for {
		_, err := client.ListMongo(v1.NamespaceAll)
		if time.Now().After(deadline) {
			return fmt.Errorf("timeout")
		}
		if apierrors.IsNotFound(err) {
			return nil
		}
		if err != nil {
			return err
		}
		time.Sleep(time.Second)
	}
	return nil
}

var _ = Describe("mongo operator general test", func() {
	var client *pkg.K8sClient
	BeforeEach(func() {
		inCluster, err := strconv.ParseBool(os.Getenv("IN_CLUSTER"))
		Expect(err).To(BeNil())
		cfg, err := pkg.NewClientConfig(inCluster)
		Expect(err).To(BeNil())
		client, err = pkg.NewK8sClient(cfg)
		Expect(err).To(BeNil())
	})
	It("should be runnable and stopable", func() {
		By("creating the pod")
		name := "mongo-operator-" + pkg.RandStringBytesRmndr(6)
		createOperatorPod(client, name)
		Expect(waitForPodRunning(client, "kube-system", name, 5*time.Minute)).To(BeNil())
		Expect(pkg.WaitForTPRReady(client.RESTClient, pkg.TPRGroup, pkg.TPRVersion, pkg.TPRMongoKind)).To(BeNil())

		By("deleting the pod")
		deleteOperatorPod(client, name)
		Expect(waitForPodToDisappear(client, "kube-system", name, 5*time.Minute)).To(BeNil())
		Expect(waitForTPRToDisappear(client, 5*time.Minute)).To(BeNil())
	})
})
