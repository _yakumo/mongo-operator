package e2e

import (
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"time"

	"github.com/caicloud/operators/mongodb/pkg"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"k8s.io/client-go/1.5/pkg/api"
	"k8s.io/client-go/1.5/pkg/api/errors"
	"k8s.io/client-go/1.5/pkg/api/v1"
	"k8s.io/client-go/1.5/pkg/labels"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

// User type for test
type User struct {
	ID        bson.ObjectId `bson:"_id,omitempty"`
	Name      string        `bson:"name"`
	Phone     string
	Timestamp time.Time
}

// ReplSetStatus of mongo
type ReplSetStatus struct {
	Members []ReplSetMember `bson:"members"`
}

// ReplSetMember of mongo
type ReplSetMember struct {
	StateStr string `bson:"stateStr"`
}

func waitForMongoReady(client *pkg.K8sClient, ns, name string, timeout time.Duration) error {
	now := time.Now()
	deadline := now.Add(timeout)
	for {
		m, err := client.GetMongo(ns, name)
		if err != nil {
			return err
		}
		if m.Status.State == pkg.RUNNING {
			break
		}
		if time.Now().After(deadline) {
			return fmt.Errorf("timeout")
		}
	}
	return nil
}

func waitForMongoToDisappear(client *pkg.K8sClient, ns, name string, timeout time.Duration) error {
	now := time.Now()
	deadline := now.Add(timeout)
	for {
		_, err := client.GetMongo(ns, name)
		if err != nil && errors.IsNotFound(err) {
			break
		}
		if time.Now().After(deadline) {
			return fmt.Errorf("timeout")
		}
	}
	return nil
}

func waitForMongoRecovering(client *pkg.K8sClient, ns, name string, timeout time.Duration) error {
	now := time.Now()
	deadline := now.Add(timeout)
	for {
		m, err := client.GetMongo(ns, name)
		if err != nil {
			return err
		}
		if m.Status.State == pkg.RECOVERING {
			break
		}
		if time.Now().After(deadline) {
			return fmt.Errorf("timeout")
		}
	}
	return nil
}

func waitForMongoReplSetSvcReady(client *pkg.K8sClient, ns, name string, timeout time.Duration) error {
	now := time.Now()
	deadline := now.Add(timeout)
	for {
		_, err := client.Core().Services(ns).Get(fmt.Sprintf("%s-mongo", name))
		if time.Now().After(deadline) {
			return fmt.Errorf("timeout")
		}
		if !errors.IsNotFound(err) {
			return err
		}
		if errors.IsNotFound(err) {
			time.Sleep(time.Second)
			continue
		}
		break
	}
	return nil
}

func waitForMongoPodsToDisappear(client *pkg.K8sClient, ns, name string, timeout time.Duration) error {
	now := time.Now()
	deadline := now.Add(timeout)
	for {
		list, err := client.Core().Pods(ns).List(api.ListOptions{
			LabelSelector: labels.SelectorFromSet(map[string]string{"mongo-replicaset-key": fmt.Sprintf("%s-mongo", name)})})
		if err != nil {
			return err
		}
		if len(list.Items) == 0 {
			break
		}
		if time.Now().After(deadline) {
			return fmt.Errorf("timeout")
		}
	}
	return nil
}

func waitForMongoReplSetSvcToDisappear(client *pkg.K8sClient, ns, name string, timeout time.Duration) error {
	now := time.Now()
	deadline := now.Add(timeout)
	for {
		list, err := client.Core().Services(ns).List(api.ListOptions{
			LabelSelector: labels.SelectorFromSet(map[string]string{"mongo-replicaset-key": fmt.Sprintf("%s-mongo", name)})})
		if err != nil {
			return err
		}
		if len(list.Items) == 0 {
			break
		}
		if time.Now().After(deadline) {
			return fmt.Errorf("timeout")
		}
	}
	return nil
}

func waitForMongoSvcsToDisappear(client *pkg.K8sClient, ns, name string, timeout time.Duration) error {
	now := time.Now()
	deadline := now.Add(timeout)
	for {
		list, err := client.Core().Services(ns).List(api.ListOptions{
			LabelSelector: labels.SelectorFromSet(map[string]string{"mongo-replicaset-key": fmt.Sprintf("%s-mongo", name)})})
		if err != nil {
			return err
		}
		if len(list.Items) == 0 {
			break
		}
		if time.Now().After(deadline) {
			return fmt.Errorf("timeout")
		}
	}
	return nil
}

func gatherRoles(result ReplSetStatus) (int, int, int) {
	primary, secondary, arbiter := 0, 0, 0
	for _, m := range result.Members {
		if m.StateStr == pkg.PRIMARY {
			primary++
		} else if m.StateStr == pkg.SECONDARY {
			secondary++
		} else if m.StateStr == pkg.ARBITER {
			arbiter++
		}
	}
	return primary, secondary, arbiter
}

func rolesShouldBe(host string, tpri, tsec, tarb int, timeout time.Duration) error {
	now := time.Now()
	deadline := now.Add(timeout)

	session, err := mgo.DialWithTimeout(host, 5*time.Second)
	if err != nil {
		return err
	}
	defer session.Close()

	for {
		result := ReplSetStatus{}
		err := session.Run("replSetGetStatus", &result)
		if err != nil {
			return err
		}
		pri, sec, arb := gatherRoles(result)
		if pri == tpri && sec == tsec && arb == tarb {
			return nil
		}
		if time.Now().After(deadline) {
			return fmt.Errorf("timeout")
		}
	}
}

var _ = Describe("mongo operator replica set test", func() {
	var client *pkg.K8sClient
	var name, ns string
	BeforeEach(func() {
		name = "mongo-operator-" + pkg.RandStringBytesRmndr(6)
		inCluster, err := strconv.ParseBool(os.Getenv("IN_CLUSTER"))
		Expect(err).To(BeNil())
		cfg, err := pkg.NewClientConfig(inCluster)
		Expect(err).To(BeNil())
		client, err = pkg.NewK8sClient(cfg)
		Expect(err).To(BeNil())
		createOperatorPod(client, name)
		Expect(waitForPodRunning(client, "kube-system", name, 5*time.Minute)).To(BeNil())
		Expect(pkg.WaitForTPRReady(client.RESTClient, pkg.TPRGroup, pkg.TPRVersion, pkg.TPRMongoKind)).To(BeNil())
		// It seems when the wait function returns, we still cannot post a new mongo to apiserver, so wait more here, tricky!
		time.Sleep(10 * time.Second)
		ns = "default" // "mong-operator-e2e-" + pkg.RandStringBytesRmndr(6)
		// namespace := v1.Namespace{
		// 	ObjectMeta: v1.ObjectMeta{
		// 		Name: ns,
		// 	},
		// }
		// _, err = client.Core().Namespaces().Create(&namespace)
		// Expect(err).To(BeNil())
	})
	AfterEach(func() {
		deleteOperatorPod(client, name)
		Expect(waitForPodToDisappear(client, "kube-system", name, 5*time.Minute)).To(BeNil())
		Expect(waitForTPRToDisappear(client, 5*time.Minute)).To(BeNil())
		// err := client.Core().Namespaces().Delete(ns, &api.DeleteOptions{})
		// Expect(err).To(BeNil())
	})
	It("should be able to keep data in mongo replica set with arbiter", func() {
		replicas := int(rand.Float64()*4 + 2)
		if replicas%2 == 1 {
			replicas++
		}
		By("creating mongo rs")
		n := "mycluster"
		rs := pkg.Mongo{
			ObjectMeta: v1.ObjectMeta{
				Name:      n,
				Namespace: ns,
			},
			Spec: pkg.MongoSpec{
				Image:    "mongo",
				Version:  "3.4",
				Replicas: replicas,
			},
		}
		_, err := client.CreateMongo(ns, &rs)
		Expect(err).To(BeNil())
		Expect(waitForMongoReady(client, ns, n, 5*time.Minute)).To(BeNil())
		Expect(waitForMongoReplSetSvcReady(client, ns, n, 5*time.Minute)).To(BeNil())

		By("checking roles")
		Expect(rolesShouldBe(fmt.Sprintf("%s-mongo.%s", n, ns), 1, replicas-1, 1, 30*time.Second)).To(BeNil())

		By("test connectivity")
		session, err := mgo.DialWithTimeout(fmt.Sprintf("%s-mongo.%s", n, ns), 5*time.Second)
		Expect(err).To(BeNil())

		By("test insert data")
		c := session.DB("test").C("users")
		err = c.Insert(&User{Name: "yakumo", Phone: "1234567890", Timestamp: time.Now()})
		Expect(err).To(BeNil())
		session.Close()

		By("check number of instances")
		list, err := client.Core().Pods(ns).List(api.ListOptions{
			LabelSelector: labels.SelectorFromSet(map[string]string{"mongo-replicaset-key": fmt.Sprintf("%s-mongo", n)})})
		pods := list.Items
		Expect(len(pods)).To(Equal(replicas + 1))

		for i := 0; i < replicas+1; i++ {
			By(fmt.Sprintf("deleting pods %d of %d", i+1, replicas+1))
			err = client.Core().Pods(ns).Delete(pods[0].Name, &api.DeleteOptions{})
			Expect(err).To(BeNil())
			Expect(waitForMongoRecovering(client, ns, n, 30*time.Second)).To(BeNil())
			Expect(waitForMongoReady(client, ns, n, 5*time.Minute)).To(BeNil())
		}

		By("test connectivity")
		session, err = mgo.DialWithTimeout(fmt.Sprintf("%s-mongo.%s", n, ns), 5*time.Second)
		Expect(err).To(BeNil())

		By("test fetch data")
		c = session.DB("test").C("users")
		result1 := []User{}
		err = c.Find(bson.M{"name": "yakumo"}).All(&result1)
		Expect(err).To(BeNil())
		Expect(len(result1)).To(Equal(1))
		Expect(result1[0].Name).To(Equal("yakumo"))
		Expect(result1[0].Phone).To(Equal("1234567890"))
		Expect(result1[0].Timestamp.Before(time.Now())).To(BeTrue())
		session.Close()

		By("checking roles")
		Expect(rolesShouldBe(fmt.Sprintf("%s-mongo.%s", n, ns), 1, replicas-1, 1, 30*time.Second)).To(BeNil())

		By("deleting mongo rs")
		err = client.DeleteMongo(ns, n, &api.DeleteOptions{})
		Expect(err).To(BeNil())
		Expect(waitForMongoPodsToDisappear(client, ns, n, 5*time.Minute)).To(BeNil())
		Expect(waitForMongoReplSetSvcToDisappear(client, ns, n, 5*time.Minute)).To(BeNil())
	})

	It("should be able to keep data in mongo replica set with no arbiter", func() {
		replicas := int(rand.Float64()*4 + 2)
		if replicas%2 == 0 {
			replicas++
		}
		By("creating mongo rs")
		n := "mycluster"
		rs := pkg.Mongo{
			ObjectMeta: v1.ObjectMeta{
				Name:      n,
				Namespace: ns,
			},
			Spec: pkg.MongoSpec{
				Image:    "mongo",
				Version:  "3.4",
				Replicas: replicas,
			},
		}
		_, err := client.CreateMongo(ns, &rs)
		Expect(err).To(BeNil())
		Expect(waitForMongoReady(client, ns, n, 5*time.Minute)).To(BeNil())
		Expect(waitForMongoReplSetSvcReady(client, ns, n, 5*time.Minute)).To(BeNil())

		By("checking roles")
		Expect(rolesShouldBe(fmt.Sprintf("%s-mongo.%s", n, ns), 1, replicas-1, 0, 30*time.Second)).To(BeNil())

		By("test connectivity")
		session, err := mgo.DialWithTimeout(fmt.Sprintf("%s-mongo.%s", n, ns), 5*time.Second)
		Expect(err).To(BeNil())

		By("test insert data")
		c := session.DB("test").C("users")
		err = c.Insert(&User{Name: "yakumo", Phone: "1234567890", Timestamp: time.Now()})
		Expect(err).To(BeNil())
		session.Close()

		By("check number of instances")
		list, err := client.Core().Pods(ns).List(api.ListOptions{
			LabelSelector: labels.SelectorFromSet(map[string]string{"mongo-replicaset-key": fmt.Sprintf("%s-mongo", n)})})
		pods := list.Items
		Expect(len(pods)).To(Equal(replicas))

		for i := 0; i < replicas; i++ {
			By(fmt.Sprintf("deleting pods %d of %d", i+1, replicas))
			err = client.Core().Pods(ns).Delete(pods[0].Name, &api.DeleteOptions{})
			Expect(err).To(BeNil())
			Expect(waitForMongoRecovering(client, ns, n, 30*time.Second)).To(BeNil())
			Expect(waitForMongoReady(client, ns, n, 5*time.Minute)).To(BeNil())
		}

		By("test connectivity")
		session, err = mgo.DialWithTimeout(fmt.Sprintf("%s-mongo.%s", n, ns), 5*time.Second)
		Expect(err).To(BeNil())

		By("test fetch data")
		c = session.DB("test").C("users")
		result1 := []User{}
		err = c.Find(bson.M{"name": "yakumo"}).All(&result1)
		Expect(err).To(BeNil())
		Expect(len(result1)).To(Equal(1))
		Expect(result1[0].Name).To(Equal("yakumo"))
		Expect(result1[0].Phone).To(Equal("1234567890"))
		Expect(result1[0].Timestamp.Before(time.Now())).To(BeTrue())
		session.Close()

		By("checking roles")
		Expect(rolesShouldBe(fmt.Sprintf("%s-mongo.%s", n, ns), 1, replicas-1, 0, 30*time.Second)).To(BeNil())

		By("deleting mongo rs")
		err = client.DeleteMongo(ns, n, &api.DeleteOptions{})
		Expect(err).To(BeNil())
		Expect(waitForMongoPodsToDisappear(client, ns, n, 5*time.Minute)).To(BeNil())
		Expect(waitForMongoReplSetSvcToDisappear(client, ns, n, 5*time.Minute)).To(BeNil())
	})

	It("should be able to resolve the race about quick recreation of replica set", func() {
		replicas := int(rand.Float64()*4 + 2)
		if replicas%2 == 0 {
			replicas++
		}
		By("creating mongo rs")
		n := "mycluster"
		rs := pkg.Mongo{
			ObjectMeta: v1.ObjectMeta{
				Name:      n,
				Namespace: ns,
			},
			Spec: pkg.MongoSpec{
				Image:    "mongo",
				Version:  "3.4",
				Replicas: replicas,
			},
		}
		_, err := client.CreateMongo(ns, &rs)
		Expect(err).To(BeNil())
		Expect(waitForMongoReady(client, ns, n, 5*time.Minute)).To(BeNil())
		Expect(waitForMongoReplSetSvcReady(client, ns, n, 5*time.Minute)).To(BeNil())

		By("deleting mongo rs")
		err = client.DeleteMongo(ns, n, &api.DeleteOptions{})
		Expect(err).To(BeNil())

		time.Sleep(10 * time.Second)

		By("recreate mongo rs")
		_, err = client.CreateMongo(ns, &rs)
		Expect(err).To(BeNil())

		By("check mongo description existance")
		Expect(waitForMongoToDisappear(client, ns, n, 1*time.Minute)).To(BeNil())

		By("check the delete")
		Expect(waitForMongoPodsToDisappear(client, ns, n, 5*time.Minute)).To(BeNil())
		Expect(waitForMongoReplSetSvcToDisappear(client, ns, n, 5*time.Minute)).To(BeNil())
	})
})
