package e2e

import (
	"fmt"
	"os"
	"strconv"
	"time"

	"github.com/caicloud/operators/mongodb/pkg"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"k8s.io/client-go/1.5/pkg/api"
	"k8s.io/client-go/1.5/pkg/api/errors"
	"k8s.io/client-go/1.5/pkg/api/v1"
	"k8s.io/client-go/1.5/pkg/labels"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func waitForMongoScaling(client *pkg.K8sClient, ns, name string, timeout time.Duration) error {
	now := time.Now()
	deadline := now.Add(timeout)
	for {
		m, err := client.GetMongo(ns, name)
		if err != nil {
			return err
		}
		if m.Status.State == pkg.SCALING {
			break
		}
		if time.Now().After(deadline) {
			return fmt.Errorf("timeout")
		}
	}
	return nil
}

func waitForMongoShardingShardsToAppear(client *pkg.K8sClient, ns, name string, shards int, timeout time.Duration) error {
	now := time.Now()
	deadline := now.Add(timeout)
	for i := 0; i < shards; i++ {
		for {
			list, err := client.Core().Pods(ns).List(api.ListOptions{
				LabelSelector: labels.SelectorFromSet(map[string]string{"mongo-replicaset-key": fmt.Sprintf("%s-shard%d-mongo", name, i)})})
			if err != nil {
				return err
			}
			if len(list.Items) == 3 {
				break
			}
			if time.Now().After(deadline) {
				return fmt.Errorf("timeout")
			}
		}
	}
	return nil
}

func waitForMongoShardingSvcReady(client *pkg.K8sClient, ns, name string, timeout time.Duration) error {
	now := time.Now()
	deadline := now.Add(timeout)
	for {
		_, err := client.Core().Services(ns).Get(fmt.Sprintf("%s-mongos", name))
		if time.Now().After(deadline) {
			return fmt.Errorf("timeout")
		}
		if !errors.IsNotFound(err) {
			return err
		}
		if errors.IsNotFound(err) {
			time.Sleep(time.Second)
			continue
		}
		break
	}
	return nil
}

func waitForMongoShardingConfigSvrToDisappear(client *pkg.K8sClient, ns, name string, timeout time.Duration) error {
	now := time.Now()
	deadline := now.Add(timeout)
	for {
		list, err := client.Core().Pods(ns).List(api.ListOptions{
			LabelSelector: labels.SelectorFromSet(map[string]string{"mongo-replicaset-key": fmt.Sprintf("%s-configsvr-mongo", name)})})
		if err != nil {
			return err
		}
		if len(list.Items) == 0 {
			break
		}
		if time.Now().After(deadline) {
			return fmt.Errorf("timeout")
		}
	}
	for {
		list, err := client.Core().Services(ns).List(api.ListOptions{
			LabelSelector: labels.SelectorFromSet(map[string]string{"mongo-replicaset-key": fmt.Sprintf("%s-configsvr-mongo", name)})})
		if err != nil {
			return err
		}
		if len(list.Items) == 0 {
			break
		}
		if time.Now().After(deadline) {
			return fmt.Errorf("timeout")
		}
	}
	return nil
}

func waitForMongoShardingShardsToDisappear(client *pkg.K8sClient, ns, name string, shards int, timeout time.Duration) error {
	now := time.Now()
	deadline := now.Add(timeout)
	for i := 0; i < shards; i++ {
		for {
			list, err := client.Core().Pods(ns).List(api.ListOptions{
				LabelSelector: labels.SelectorFromSet(map[string]string{"mongo-replicaset-key": fmt.Sprintf("%s-shard%d-mongo", name, i)})})
			if err != nil {
				return err
			}
			if len(list.Items) == 0 {
				break
			}
			if time.Now().After(deadline) {
				return fmt.Errorf("timeout")
			}
		}
	}
	for i := 0; i < shards; i++ {
		for {
			list, err := client.Core().Services(ns).List(api.ListOptions{
				LabelSelector: labels.SelectorFromSet(map[string]string{"mongo-replicaset-key": fmt.Sprintf("%s-shard%d-mongo", name, i)})})
			if err != nil {
				return err
			}
			if len(list.Items) == 0 {
				break
			}
			if time.Now().After(deadline) {
				return fmt.Errorf("timeout")
			}
		}
	}
	return nil
}

func waitForMongoShardingMongosToDisappear(client *pkg.K8sClient, ns, name string, timeout time.Duration) error {
	now := time.Now()
	deadline := now.Add(timeout)
	for {
		list, err := client.Core().Pods(ns).List(api.ListOptions{
			LabelSelector: labels.SelectorFromSet(map[string]string{"mongos-proxy-key": fmt.Sprintf("%s-mongos", name)})})
		if err != nil {
			return nil
		}
		if len(list.Items) == 0 {
			break
		}
		if time.Now().After(deadline) {
			return fmt.Errorf("timeout")
		}
	}
	for {
		list, err := client.Core().Services(ns).List(api.ListOptions{
			LabelSelector: labels.SelectorFromSet(map[string]string{"mongos-proxy-key": fmt.Sprintf("%s-mongos", name)})})
		if err != nil {
			return nil
		}
		if len(list.Items) == 0 {
			break
		}
		if time.Now().After(deadline) {
			return fmt.Errorf("timeout")
		}
	}
	return nil
}

var _ = Describe("mongo operator sharding test", func() {
	var client *pkg.K8sClient
	var name, ns string
	BeforeEach(func() {
		name = "mongo-operator-" + pkg.RandStringBytesRmndr(6)
		inCluster, err := strconv.ParseBool(os.Getenv("IN_CLUSTER"))
		Expect(err).To(BeNil())
		cfg, err := pkg.NewClientConfig(inCluster)
		Expect(err).To(BeNil())
		client, err = pkg.NewK8sClient(cfg)
		Expect(err).To(BeNil())
		createOperatorPod(client, name)
		Expect(waitForPodRunning(client, "kube-system", name, 5*time.Minute)).To(BeNil())
		Expect(pkg.WaitForTPRReady(client.RESTClient, pkg.TPRGroup, pkg.TPRVersion, pkg.TPRMongoKind)).To(BeNil())
		// It seems when the wait function returns, we still cannot post a new mongo to apiserver, so wait more here, tricky!
		time.Sleep(10 * time.Second)
		ns = "default" // "mong-operator-e2e-" + pkg.RandStringBytesRmndr(6)
		// namespace := v1.Namespace{
		// 	ObjectMeta: v1.ObjectMeta{
		// 		Name: ns,
		// 	},
		// }
		// _, err = client.Core().Namespaces().Create(&namespace)
		// Expect(err).To(BeNil())
	})
	AfterEach(func() {
		deleteOperatorPod(client, name)
		Expect(waitForPodToDisappear(client, "kube-system", name, 5*time.Minute)).To(BeNil())
		Expect(waitForTPRToDisappear(client, 5*time.Minute)).To(BeNil())
		// err := client.Core().Namespaces().Delete(ns, &api.DeleteOptions{})
		// Expect(err).To(BeNil())
	})
	It("should be able to create/delete mongo sharding cluster", func() {
		By("creating mongo sharding cluster")
		n := "mycluster"
		cluster := pkg.Mongo{
			ObjectMeta: v1.ObjectMeta{
				Name:      n,
				Namespace: ns,
			},
			Spec: pkg.MongoSpec{
				Image:   "mongo",
				Version: "3.4",
				Sharding: &pkg.MongoShardingConfig{
					Shards: 2,
					Collections: []pkg.ShardCollection{
						pkg.ShardCollection{
							DB:         "test",
							Collection: "user",
							Key:        "name",
							Hashed:     true,
						},
					},
				},
			},
		}
		_, err := client.CreateMongo(ns, &cluster)
		Expect(err).To(BeNil())
		Expect(waitForMongoReady(client, ns, n, 10*time.Minute)).To(BeNil())
		Expect(waitForMongoShardingSvcReady(client, ns, n, 5*time.Minute)).To(BeNil())

		By("test connectivity")
		session, err := mgo.DialWithTimeout(fmt.Sprintf("%s-mongos.%s", n, ns), 5*time.Second)
		Expect(err).To(BeNil())

		By("test insert data")
		c := session.DB("test").C("user")
		for i := 0; i < 10000; i++ {
			err = c.Insert(&User{Name: fmt.Sprintf("yakumo%d", i), Phone: "1234567890", Timestamp: time.Now()})
			Expect(err).To(BeNil())
		}
		session.Close()

		By("test connectivity")
		session, err = mgo.DialWithTimeout(fmt.Sprintf("%s-mongos.%s", n, ns), 5*time.Second)
		Expect(err).To(BeNil())
		c = session.DB("test").C("user")

		By("test fetch data")
		result := []User{}
		err = c.Find(bson.M{"phone": "1234567890"}).All(&result)
		Expect(err).To(BeNil())
		Expect(len(result)).To(Equal(10000))
		session.Close()

		By("test fetch data from shards")
		session, err = mgo.DialWithTimeout(fmt.Sprintf("%s-shard0-mongo.%s", n, ns), 5*time.Second)
		c = session.DB("test").C("user")
		Expect(err).To(BeNil())
		result0 := []User{}
		err = c.Find(bson.M{"phone": "1234567890"}).All(&result0)
		Expect(err).To(BeNil())
		Expect(len(result0)).To(BeNumerically(">", 0))
		session.Close()

		session, err = mgo.DialWithTimeout(fmt.Sprintf("%s-shard1-mongo.%s", n, ns), 5*time.Second)
		c = session.DB("test").C("user")
		Expect(err).To(BeNil())
		result1 := []User{}
		err = c.Find(bson.M{"phone": "1234567890"}).All(&result1)
		Expect(err).To(BeNil())
		Expect(len(result1)).To(BeNumerically(">", 0))
		session.Close()

		Expect(len(result0) + len(result1)).To(Equal(10000))

		By("deleting mongo sharding cluster")
		err = client.DeleteMongo(ns, n, &api.DeleteOptions{})
		Expect(err).To(BeNil())
		Expect(waitForMongoShardingConfigSvrToDisappear(client, ns, n, 5*time.Minute)).To(BeNil())
		Expect(waitForMongoShardingMongosToDisappear(client, ns, n, 5*time.Minute)).To(BeNil())
		Expect(waitForMongoShardingShardsToDisappear(client, ns, n, 2, 5*time.Minute)).To(BeNil())
	})

	It("should be able to scale out mongo sharding cluster", func() {
		By("creating mongo sharding cluster")
		n := "mycluster"
		cluster := pkg.Mongo{
			ObjectMeta: v1.ObjectMeta{
				Name:      n,
				Namespace: ns,
			},
			Spec: pkg.MongoSpec{
				Image:   "mongo",
				Version: "3.4",
				Sharding: &pkg.MongoShardingConfig{
					Shards: 2,
					Collections: []pkg.ShardCollection{
						pkg.ShardCollection{
							DB:         "test",
							Collection: "user",
							Key:        "name",
							Hashed:     true,
						},
					},
				},
			},
		}
		_, err := client.CreateMongo(ns, &cluster)
		Expect(err).To(BeNil())
		Expect(waitForMongoReady(client, ns, n, 10*time.Minute)).To(BeNil())
		Expect(waitForMongoShardingSvcReady(client, ns, n, 5*time.Minute)).To(BeNil())
		Expect(waitForMongoShardingShardsToAppear(client, ns, n, 2, 5*time.Minute)).To(BeNil())
		status, err := client.GetMongo(ns, n)
		Expect(err).To(BeNil())
		Expect(len(status.ShardingStatus.Shards)).To(Equal(2))

		By("scale out")
		cluster.Spec.Sharding.Shards = 3
		_, err = client.UpdateMongo(ns, n, &cluster)
		Expect(err).To(BeNil())
		Expect(waitForMongoScaling(client, ns, n, 30*time.Second)).To(BeNil())
		Expect(waitForMongoShardingShardsToAppear(client, ns, n, 3, 5*time.Minute)).To(BeNil())
		Expect(waitForMongoReady(client, ns, n, 10*time.Minute)).To(BeNil())
		status, err = client.GetMongo(ns, n)
		Expect(err).To(BeNil())
		Expect(len(status.ShardingStatus.Shards)).To(Equal(3))

		By("deleting mongo sharding cluster")
		err = client.DeleteMongo(ns, n, &api.DeleteOptions{})
		Expect(err).To(BeNil())
		Expect(waitForMongoShardingConfigSvrToDisappear(client, ns, n, 5*time.Minute)).To(BeNil())
		Expect(waitForMongoShardingMongosToDisappear(client, ns, n, 5*time.Minute)).To(BeNil())
		Expect(waitForMongoShardingShardsToDisappear(client, ns, n, 3, 5*time.Minute)).To(BeNil())
	})
})
