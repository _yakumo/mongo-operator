apt-get update
apt-get install -y vim wget git
cd /tmp
wget https://storage.googleapis.com/golang/go1.7.4.linux-amd64.tar.gz
tar -C /usr/local -xzf go1.7.4.linux-amd64.tar.gz
export PATH=$PATH:/usr/local/go/bin
export GOPATH=~/gocode
mkdir -p ~/gocode/src/github.com/caicloud
cd ~/gocode/src/github.com/caicloud
git clone git@github.com:caicloud/operators.git
