package main

import (
	"flag"
	"math/rand"
	"os"
	"os/signal"
	"syscall"
	"time"

	"bitbucket.org/_yakumo/mongo-operator/pkg"
	"github.com/golang/glog"
	"github.com/spf13/pflag"
)

var (
	flags     = pflag.NewFlagSet("", pflag.ExitOnError)
	inCluster = flags.Bool("running-in-cluster", true,
		`Optional, if this controller is running in a kubernetes cluster, use the
		pod secrets for creating a Kubernetes client.`)
	master = flags.String("master", "", "kubernetes master url")
)

func main() {
	rand.Seed(time.Now().UTC().UnixNano())
	flags.AddGoFlagSet(flag.CommandLine)
	flags.Parse(os.Args)

	// Workaround of noisy log, see https://github.com/kubernetes/kubernetes/issues/17162
	flag.CommandLine.Parse([]string{})

	operator, err := pkg.NewOperator(*inCluster)
	if err != nil {
		glog.Fatalf("create mongo operator failed: %v", err)
	}

	go handleSigterm(operator)
	operator.Run()
}

func handleSigterm(o *pkg.Operator) {
	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, syscall.SIGTERM, syscall.SIGINT)
	<-signalChan
	glog.Infof("Received SIGTERM, shutting down")

	exitCode := 0
	if err := o.Stop(); err != nil {
		glog.Infof("Error during shutdown %v", err)
		exitCode = 1
	}

	glog.Infof("Exiting with %v", exitCode)
	os.Exit(exitCode)
}
