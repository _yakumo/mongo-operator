package pkg

import (
	"encoding/json"

	"k8s.io/client-go/1.5/kubernetes"
	"k8s.io/client-go/1.5/pkg/api"
	"k8s.io/client-go/1.5/pkg/api/unversioned"
	"k8s.io/client-go/1.5/pkg/labels"
	"k8s.io/client-go/1.5/pkg/runtime"
	"k8s.io/client-go/1.5/pkg/runtime/serializer"
	"k8s.io/client-go/1.5/pkg/watch"
	"k8s.io/client-go/1.5/rest"
	"k8s.io/client-go/1.5/tools/cache"
	"k8s.io/client-go/1.5/tools/clientcmd"
)

// NewClientConfig return the config according to inCluster flag
func NewClientConfig(inCluster bool) (*rest.Config, error) {
	if inCluster {
		return rest.InClusterConfig()
	}
	return clientcmd.BuildConfigFromKubeconfigGetter("", clientcmd.NewDefaultClientConfigLoadingRules().Load)
}

type mongoDecoder struct {
	dec   *json.Decoder
	close func() error
}

func (d *mongoDecoder) Close() {
	d.close()
}

func (d *mongoDecoder) Decode() (action watch.EventType, object runtime.Object, err error) {
	var e struct {
		Type   watch.EventType
		Object Mongo
	}
	if err := d.dec.Decode(&e); err != nil {
		return watch.Error, nil, err
	}
	return e.Type, &e.Object, nil
}

// K8sClient is wrapped client for operating k8s and also mongo resource
type K8sClient struct {
	*kubernetes.Clientset
	RESTClient *rest.RESTClient
}

// NewK8sClient accepts client config and return wrapped client
func NewK8sClient(c *rest.Config) (*K8sClient, error) {
	client, err := kubernetes.NewForConfig(c)
	if err != nil {
		return nil, err
	}
	configShallowCopy := *c
	configShallowCopy.APIPath = "/apis"
	configShallowCopy.GroupVersion = &unversioned.GroupVersion{
		Group:   "infra.caicloud.io",
		Version: "v1",
	}
	// TODO(fabxc): is this even used with our custom list/watch functions?
	configShallowCopy.NegotiatedSerializer = serializer.DirectCodecFactory{CodecFactory: api.Codecs}
	restClient, err := rest.RESTClientFor(&configShallowCopy)
	if err != nil {
		return nil, err
	}
	return &K8sClient{Clientset: client, RESTClient: restClient}, nil
}

// GetMongo tpr
func (c *K8sClient) GetMongo(ns, name string) (*Mongo, error) {
	req := c.RESTClient.Get().Namespace(ns).Resource("mongos").Name(name).FieldsSelectorParam(nil)
	b, err := req.DoRaw()
	if err != nil {
		return nil, err
	}
	var m Mongo
	return &m, json.Unmarshal(b, &m)
}

// CreateMongo tpr
func (c *K8sClient) CreateMongo(ns string, body *Mongo) (*Mongo, error) {
	req := c.RESTClient.Post().Namespace(ns).Resource("mongos").Body(body)
	b, err := req.DoRaw()
	if err != nil {
		return nil, err
	}
	var m Mongo
	return &m, json.Unmarshal(b, &m)
}

// UpdateMongo tpr
func (c *K8sClient) UpdateMongo(ns, name string, body *Mongo) (*Mongo, error) {
	req := c.RESTClient.Put().Namespace(ns).Resource("mongos").Name(name).Body(body)
	b, err := req.DoRaw()
	if err != nil {
		return nil, err
	}
	var m Mongo
	return &m, json.Unmarshal(b, &m)
}

// UpdateMongoStatus update tpr status
func (c *K8sClient) UpdateMongoStatus(ns, name string, body *Mongo) (*Mongo, error) {
	req := c.RESTClient.Put().Namespace(ns).Resource("mongos").Name(name).SubResource("status").Body(body)
	b, err := req.DoRaw()
	if err != nil {
		return nil, err
	}
	var m Mongo
	return &m, json.Unmarshal(b, &m)
}

// DeleteMongo tpr
func (c *K8sClient) DeleteMongo(ns string, name string, options *api.DeleteOptions) error {
	req := c.RESTClient.Delete().Namespace(ns).Resource("mongos").Name(name).Body(options)
	_, err := req.DoRaw()
	return err
}

// ListMongo tpr
func (c *K8sClient) ListMongo(ns string) (*MongoList, error) {
	req := c.RESTClient.Get().Namespace(ns).Resource("mongos").FieldsSelectorParam(nil)
	b, err := req.DoRaw()
	if err != nil {
		return nil, err
	}
	var ml MongoList
	return &ml, json.Unmarshal(b, &ml)
}

// WatchMongo tpr
func (c *K8sClient) WatchMongo(ns string) (*watch.StreamWatcher, error) {
	r, err := c.RESTClient.Get().Prefix("watch").Namespace(ns).Resource("mongos").FieldsSelectorParam(nil).Stream()
	if err != nil {
		return nil, err
	}
	return watch.NewStreamWatcher(&mongoDecoder{
		dec:   json.NewDecoder(r),
		close: r.Close,
	}), nil
}

// NewMongoListWatch return listwatch structure of mongo thirdpartyresource
func NewMongoListWatch(client *K8sClient) *cache.ListWatch {
	listFunc := func(options api.ListOptions) (runtime.Object, error) {
		return client.ListMongo(api.NamespaceAll)
	}
	watchFunc := func(options api.ListOptions) (watch.Interface, error) {
		return client.WatchMongo(api.NamespaceAll)
	}
	return &cache.ListWatch{
		ListFunc:  listFunc,
		WatchFunc: watchFunc,
	}
}

// NewListWatchFromClient return listwatch structure for original resource in k8s
func NewListWatchFromClient(c cache.Getter, namespace, resource string, selector labels.Selector) *cache.ListWatch {
	listFunc := func(options api.ListOptions) (runtime.Object, error) {
		options.LabelSelector = selector
		return c.Get().
			Namespace(namespace).
			Resource(resource).
			VersionedParams(&options, api.ParameterCodec).
			FieldsSelectorParam(nil).
			Do().
			Get()
	}
	watchFunc := func(options api.ListOptions) (watch.Interface, error) {
		options.LabelSelector = selector
		return c.Get().
			Prefix("watch").
			Namespace(namespace).
			Resource(resource).
			VersionedParams(&options, api.ParameterCodec).
			FieldsSelectorParam(nil).
			Watch()
	}
	return &cache.ListWatch{ListFunc: listFunc, WatchFunc: watchFunc}
}
