package pkg

import (
	"encoding/json"
	"fmt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"io/ioutil"
	"os"
	"os/exec"
	"time"

	"github.com/golang/glog"
)

var (
	cmds = map[string]string{
		"ping": "printjson(db.runCommand(\"ping\"))",
		"dbStatus": `result = db.stats()
delete result["$gleStats"]
printjson(result)
`,
		"rsStatus": `result = rs.status()
delete result["$gleStats"]
result.date = result.date.toJSON()
result.term = result.term.toNumber()
result.heartbeatIntervalMillis = result.heartbeatIntervalMillis.toNumber()
delete result["optimes"]
for (i in result.members) {
  delete result.members[i].optime
  delete result.members[i].optimeDurable
  delete result.members[i].pingMs
  if (result.members[i].optimeDate) {
    result.members[i].optimeDate = result.members[i].optimeDate.toJSON()
  }
  if (result.members[i].optimeDurableDate) {
    result.members[i].optimeDurableDate = result.members[i].optimeDurableDate.toJSON()
  }
  if (result.members[i].electionTime) {
    result.members[i].electionTime = result.members[i].electionTime.getTime()
  }
  if (result.members[i].electionDate) {
    result.members[i].electionDate = result.members[i].electionDate.toJSON() 
  }
  if (result.members[i].lastHeartbeat) {
    result.members[i].lastHeartbeat = result.members[i].lastHeartbeat.toJSON()
  }
  if (result.members[i].lastHeartbeatRecv) {
    result.members[i].lastHeartbeatRecv = result.members[i].lastHeartbeatRecv.toJSON()
  }
}
printjson(result)
`,
		"rsInitiate": `result = rs.initiate(%s)
delete result["$gleStats"]
printjson(result)
`,
		"rsAddMember":       "printjson(rs.add(\"%s\"))",
		"rsAddArb":          "printjson(rs.addArb(\"%s\"))",
		"rsRemove":          "printjson(rs.remove(\"%s\"))",
		"shAddShard":        "printjson(sh.addShard(\"%s\"))",
		"shShardCollection": "printjson(sh.shardCollection(\"%s\", %s, %t))",
		"shEnableSharding":  "printjson(sh.enableSharding(\"%s\"))",
	}
)

func runMongoShell(host, key string, arg ...interface{}) ([]byte, error) {
	if _, ok := cmds[key]; !ok {
		return nil, fmt.Errorf("unsupported command")
	}
	narg := make([]interface{}, len(arg))
	for i, v := range arg {
		narg[i] = v
	}
	// TODO: currently we use timestamp here to create file
	// maybe we can find a better thread-safe method to do so
	fileName := fmt.Sprintf("%d.js", time.Now().UnixNano())
	defer os.Remove(fileName)
	for i := 0; i < 3; i++ {
		err := ioutil.WriteFile(fileName, []byte(fmt.Sprintf(cmds[key], narg...)), 0644)
		glog.V(4).Infof("%s", fmt.Sprintf(cmds[key], narg...))
		if err == nil {
			break
		}
		if i >= 2 {
			return nil, fmt.Errorf("create cmd script failed: %v", err)
		}
		glog.Infof("create cmd script failed: %v, retrying: %d", err, i)
		time.Sleep(time.Second)
	}
	for i := 0; i < 3; i++ {
		cmd := exec.Command("mongo", fmt.Sprintf("mongodb://%s", host), fileName, "--quiet")
		stderr, err := cmd.StderrPipe()
		if err != nil {
			glog.Infof("create stderr pipe failed")
			continue
		}
		ret, err := cmd.Output()
		if err == nil {
			return ret, err
		}
		if i >= 2 {
			return nil, err
		}
		e := []byte{}
		stderr.Read(e)
		glog.Infof("execute mongo shell failed: %v, stderr: %s, retrying: %d", err, string(e), i)
		time.Sleep(time.Second)
	}
	return nil, fmt.Errorf("execute mongo shell failed")
}

type mongoReplSetInitResp struct {
	Info2  string `json:"info2,omitempty"`
	Info   string `json:"info,omitempty"`
	Me     string `json:"me,omitempty"`
	Ok     int    `json:"ok,omitempty"`
	ErrMsg string `json:"errmsg,omitempty"`
	Code   int    `json:"code,omitempty"`
}

type mongoReplSetStatusResp struct {
	Set                     string               `json:"set,omitempty"`
	Date                    string               `json:"date,omitempty"`
	MyState                 int                  `json:"myState,omitempty"`
	Term                    int64                `json:"term,omitempty"`
	HeartbeatIntervalMillis int64                `json:"heartbeatIntervalMillis,omitempty"`
	Members                 []mongoReplSetMember `json:"members,omitempty"`
	Ok                      int                  `json:"ok,omitempty"`
	ErrMsg                  string               `json:"errmsg,omitempty"`
}

type mongoReplSetMember struct {
	ID                int    `json:"_id,omitempty"`
	Name              string `json:"name,omitempty"`
	Health            int    `json:"health,omitempty"`
	State             int    `json:"state,omitempty"`
	StateStr          string `json:"stateStr,omitempty"`
	Uptime            int    `json:"uptime,omitempty"`
	Optime            int    `json:"optime,omitempty"`
	OptimeDate        string `json:"optimeDate,omitempty"`
	ElectionTime      int    `json:"electionTime,omitempty"`
	ElectionDate      string `json:"electionDate,omitempty"`
	LastHeartbeat     string `json:"lastHeartbeat,omitempty"`
	LastHeartbeatRecv string `json:"lastHeartbeatRecv,omitempty"`
	ConfigVersion     int    `json:"configVerson,omitempty"`
	Self              bool   `json:"self,omitempty"`
}

type mongoGeneralResp struct {
	Ok       int    `json:"ok,omitempty"`
	ErrMsg   string `json:"errmsg,omitempty"`
	Code     int    `json:"code,omitempty"`
	CodeName string `json:"codeName,omitempty"`
}

func mongoShellPing(host string) error {
	raw, err := runMongoShell(host, "ping")
	if err != nil {
		glog.Errorf("%v", err)
		return err
	}
	resp := mongoGeneralResp{}
	err = json.Unmarshal(raw, &resp)
	if err != nil {
		glog.Errorf("%v, %v, %v", string(raw), resp, err)
		return err
	}
	if resp.Ok != 1 {
		return fmt.Errorf("mongo ping failed: %s", resp.ErrMsg)
	}
	return nil
}

func mongoShellDBStats(host string) error {
	raw, err := runMongoShell(host, "dbStatus")
	if err != nil {
		glog.Errorf("%v", err)
		return err
	}
	resp := mongoGeneralResp{}
	err = json.Unmarshal(raw, &resp)
	if err != nil {
		glog.Errorf("%v, %v, %v", string(raw[:len(raw)]), resp, err)
		return err
	}
	if resp.Ok != 1 {
		return fmt.Errorf("mongo fetch db stats failed: %s", resp.ErrMsg)
	}
	return nil
}

type mongoReplSetInitConfig struct {
	ID        string                     `json:"_id"`
	ConfigSvr bool                       `json:"configsvr"`
	Members   []mongoReplSetMemberConfig `json:"members"`
}

type mongoReplSetMemberConfig struct {
	ID      int    `json:"_id"`
	Host    string `json:"host"`
	Arbiter bool   `json:"arbiterOnly"`
}

func mongoShellReplSetInit(primary string, rs string, configsvr bool, arbiter bool, hosts []string) error {
	members := []mongoReplSetMemberConfig{}
	for idx, host := range hosts {
		member := mongoReplSetMemberConfig{
			ID:      idx,
			Host:    host,
			Arbiter: false,
		}
		if arbiter && idx == len(hosts)-1 {
			member.Arbiter = true
		}
		members = append(members, member)
	}
	config := mongoReplSetInitConfig{ID: rs, ConfigSvr: configsvr, Members: members}
	req, err := json.Marshal(config)
	if err != nil {
		return err
	}
	raw, err := runMongoShell(primary, "rsInitiate", string(req))
	if err != nil {
		return err
	}
	resp := mongoReplSetInitResp{}
	err = json.Unmarshal(raw, &resp)
	if err != nil {
		return err
	}
	if resp.Ok != 1 {
		// TODO: It seems that here probably would have error of
		// "replSetInitiate quorum check failed because not all proposed set members responded affirmatively: ${HOST}:${PORT} failed with HostUnreachable"
		// which will lead to the cluster become Broken...
		// a better retry implementation is needed
		time.Sleep(10 * time.Second)
		raw, err := runMongoShell(primary, "rsInitiate", string(req))
		if err != nil {
			return err
		}
		resp := mongoReplSetInitResp{}
		err = json.Unmarshal(raw, &resp)
		if err != nil {
			return err
		}
		if resp.Ok != 1 {
			return fmt.Errorf("mongo replica set init failed: %s", resp.ErrMsg)
		}
	}
	return nil
}

func mongoShellReplSetStatus(host string) (*mongoReplSetStatusResp, error) {
	raw, err := runMongoShell(host, "rsStatus")
	if err != nil {
		return nil, err
	}
	resp := mongoReplSetStatusResp{}
	err = json.Unmarshal(raw, &resp)
	if err != nil {
		return nil, err
	}
	if resp.Ok != 1 {
		return &resp, fmt.Errorf("mongo fetch replica set status failed: %s", resp.ErrMsg)
	}
	return &resp, nil
}

func mongoShellReplSetAddMember(host, member string) error {
	raw, err := runMongoShell(host, "rsAddMember", member)
	if err != nil {
		return err
	}
	resp := mongoGeneralResp{}
	err = json.Unmarshal(raw, &resp)
	if err != nil {
		return err
	}
	if resp.Ok != 1 {
		return fmt.Errorf("mongo add member failed: %s", resp.ErrMsg)
	}
	return err
}

func mongoShellReplSetAddArbiter(host, arbiter string) error {
	raw, err := runMongoShell(host, "rsAddArb", arbiter)
	if err != nil {
		return err
	}
	resp := mongoGeneralResp{}
	err = json.Unmarshal(raw, &resp)
	if err != nil {
		return err
	}
	if resp.Ok != 1 {
		return fmt.Errorf("mongo add arbiter failed: %s", resp.ErrMsg)
	}
	return err
}

func mongoShellReplSetRemoveMember(host, member string) error {
	raw, err := runMongoShell(host, "rsRemove", member)
	if err != nil {
		return err
	}
	resp := mongoGeneralResp{}
	err = json.Unmarshal(raw, &resp)
	if err != nil {
		return err
	}
	if resp.Ok != 1 {
		return fmt.Errorf("mongo remove member failed: %s", resp.ErrMsg)
	}
	return err
}

func mongoShellAddShard(host, rs string) error {
	raw, err := runMongoShell(host, "shAddShard", rs)
	if err != nil {
		return err
	}
	resp := mongoGeneralResp{}
	err = json.Unmarshal(raw, &resp)
	if err != nil {
		return err
	}
	if resp.Ok != 1 {
		return fmt.Errorf("mongo add shard failed: %s", resp.ErrMsg)
	}
	return err
}

func mongoShellShardCollection(host, namespace, key string, unique bool) error {
	raw, err := runMongoShell(host, "shShardCollection", namespace, key, unique)
	if err != nil {
		return err
	}
	resp := mongoGeneralResp{}
	err = json.Unmarshal(raw, &resp)
	if err != nil {
		return err
	}
	if resp.Ok != 1 && !(resp.Ok == 0 && resp.Code == 20) {
		return fmt.Errorf("mongo shard collection failed: %s", resp.ErrMsg)
	}
	return err
}

func mongoShellEnableSharding(host, db string) error {
	raw, err := runMongoShell(host, "shEnableSharding", db)
	if err != nil {
		return err
	}
	resp := mongoGeneralResp{}
	err = json.Unmarshal(raw, &resp)
	if err != nil {
		return err
	}
	if resp.Ok != 1 && !(resp.Ok == 0 && resp.Code == 23) {
		return fmt.Errorf("mongo enable sharding failed: %s", resp.ErrMsg)
	}
	return err
}

func mgoReplSetStatus(host string) (bson.M, error) {
	session, err := mgo.DialWithTimeout(host, time.Second)
	if err != nil {
		return nil, err
	}
	defer session.Close()
	result := bson.M{}
	err = session.Run(bson.D{{"replSetGetStatus", 1}}, &result)
	if err != nil {
		return nil, err
	}
	return result, nil
}
