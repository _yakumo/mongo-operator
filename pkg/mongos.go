package pkg

// TODO: make more robust

import (
	"fmt"
	"reflect"
	"sync"
	"time"

	"bitbucket.org/_yakumo/mongo-operator/pkg/queue"
	"github.com/golang/glog"
	"k8s.io/client-go/1.5/pkg/api"
	apierrors "k8s.io/client-go/1.5/pkg/api/errors"
	"k8s.io/client-go/1.5/pkg/api/v1"
	"k8s.io/client-go/1.5/pkg/labels"
	"k8s.io/client-go/1.5/tools/cache"
)

// MongosKeeper holds a or more mongos proxy
type MongosKeeper struct {
	kind         string
	desc         *Mongos
	client       *K8sClient
	stopCh       chan struct{}
	stopLock     sync.Mutex
	shutdown     bool
	podInf       cache.SharedIndexInformer
	svcInf       cache.SharedIndexInformer
	queue        *queue.Queue
	svcSyncQueue *queue.Queue
}

// NewMongosKeeper returns a mongos keeper
func NewMongosKeeper(client *K8sClient, desc *Mongos) *MongosKeeper {
	desc.Status.State = INITIALIZING
	keeper := MongosKeeper{
		kind:         "mongos",
		desc:         desc,
		client:       client,
		stopCh:       make(chan struct{}),
		queue:        queue.New(),
		svcSyncQueue: queue.New(),
	}
	keeper.podInf = cache.NewSharedIndexInformer(
		NewListWatchFromClient(keeper.client.Core().GetRESTClient(),
			desc.Namespace,
			"pods",
			labels.SelectorFromSet(map[string]string{mongosLabel: fmt.Sprintf("%s-mongos", desc.Name)})),
		&v1.Pod{}, resyncPeriod, cache.Indexers{},
	)
	keeper.podInf.AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc:    func(obj interface{}) { keeper.enqueue(obj); keeper.svcEnqueue(obj) },
		DeleteFunc: func(obj interface{}) { keeper.enqueue(obj); keeper.svcEnqueue(obj) },
		UpdateFunc: func(old, cur interface{}) {
			if !reflect.DeepEqual(old, cur) {
				keeper.enqueue(cur)
				keeper.svcEnqueue(cur)
			}
		},
	})
	keeper.svcInf = cache.NewSharedIndexInformer(
		NewListWatchFromClient(keeper.client.Core().GetRESTClient(),
			desc.Namespace,
			"services",
			labels.SelectorFromSet(map[string]string{mongosLabel: fmt.Sprintf("%s-mongos", desc.Name)})),
		&v1.Service{}, resyncPeriod, cache.Indexers{},
	)
	keeper.svcInf.AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc:    func(obj interface{}) { keeper.svcEnqueue(obj) },
		DeleteFunc: func(obj interface{}) { keeper.svcEnqueue(obj) },
		UpdateFunc: func(old, cur interface{}) {
			if !reflect.DeepEqual(old, cur) {
				keeper.svcEnqueue(cur)
			}
		},
	})
	return &keeper
}

func (k *MongosKeeper) keyFunc(obj interface{}) (string, bool) {
	key, err := cache.DeletionHandlingMetaNamespaceKeyFunc(obj)
	if err != nil {
		glog.Errorf("create key failed: %v", err)
		return key, false
	}
	return key, true
}

func (k *MongosKeeper) enqueue(obj interface{}) {
	if obj == nil {
		return
	}
	key, ok := obj.(string)
	if !ok {
		key, ok = k.keyFunc(obj)
		if !ok {
			return
		}
	}
	k.queue.Add(key)
}

func (k *MongosKeeper) svcEnqueue(obj interface{}) {
	if obj == nil {
		return
	}
	key, ok := obj.(string)
	if !ok {
		key, ok = k.keyFunc(obj)
		if !ok {
			return
		}
	}
	k.svcSyncQueue.Add(key)
}

func (k *MongosKeeper) syncStateworker() {
	for {
		key, quit := k.queue.Get()
		if quit {
			return
		}
		if err := k.syncState(key.(string)); err != nil {
			glog.Errorf("reconciliation failed, re-enqueueing: %s", err)
			// We only mark the item as done after waiting. In the meantime
			// other items can be processed but the same item won't be processed again.
			// This is a trivial form of rate-limiting that is sufficient for our throughput
			// and latency expectations.
			go func() {
				time.Sleep(3 * time.Second)
				k.queue.Done(key)
				k.queue.Add(key)
			}()
			continue
		}
		k.queue.Done(key)
	}
}

func (k *MongosKeeper) syncSvcWorker() {
	for {
		key, quit := k.svcSyncQueue.Get()
		if quit {
			return
		}
		if err := k.syncSvc(key.(string)); err != nil {
			glog.Errorf("reconciliation failed, re-enqueueing: %s", err)
			// We only mark the item as done after waiting. In the meantime
			// other items can be processed but the same item won't be processed again.
			// This is a trivial form of rate-limiting that is sufficient for our throughput
			// and latency expectations.
			go func() {
				time.Sleep(3 * time.Second)
				k.svcSyncQueue.Done(key)
				k.svcSyncQueue.Add(key)
			}()
			continue
		}
		k.svcSyncQueue.Done(key)
	}
}

func (k *MongosKeeper) isInSync() bool {
	return k.podInf.HasSynced() && k.svcInf.HasSynced()
}

func (k *MongosKeeper) recov() error {
	if !k.isInSync() {
		return fmt.Errorf("deferring sync till informer has synced")
	}
	if len(k.podInf.GetIndexer().List()) < k.desc.Spec.Size {
		pod := makeMongosPod(k.desc.Namespace, k.desc.Name, k.desc.Spec.ConfigSvr, k.desc.Spec.Image, k.desc.Spec.Version)
		pod, err := k.client.Core().Pods(pod.Namespace).Create(pod)
		if err != nil {
			return err
		}
		pod, err = waitForMongosPodRunning(k.client, pod.Namespace, pod.Name)
		if err != nil {
			return err
		}
	}
	hosts := []string{}
	for _, obj := range k.podInf.GetIndexer().List() {
		pod := obj.(*v1.Pod)
		hosts = append(hosts, pod.Status.PodIP)
	}
	if k.desc.Status.State == INITIALIZING {
		k.desc.Status.State = RUNNING
	}
	k.desc.Status.Hosts = hosts
	return nil
}

func (k *MongosKeeper) syncState(key string) error {
	switch k.desc.Status.State {
	case INITIALIZING:
		fallthrough
	case RUNNING:
		return k.recov()
	case DELETING:
		fallthrough
	case DELETED:
		fallthrough
	default:
		return nil
	}
}

func (k *MongosKeeper) syncSvc(key string) error {
	if !k.isInSync() {
		return fmt.Errorf("deferring sync till informer has synced")
	}
	if len(k.podInf.GetIndexer().List()) != 0 {
		pod := k.podInf.GetIndexer().List()[0].(*v1.Pod)
		if _, err := k.client.Core().Services(k.desc.Namespace).Get(fmt.Sprintf("%s-mongos", k.desc.Name)); err != nil {
			if apierrors.IsNotFound(err) {
				svc := makeMongosSvc(pod)
				if _, err := k.client.Core().Services(svc.Namespace).Create(svc); err != nil && !apierrors.IsAlreadyExists(err) {
					return err
				}
			} else {
				return err
			}
		}
	} else if err := k.client.Core().Services(k.desc.Namespace).Delete(fmt.Sprintf("%s-mongos", k.desc.Name), &api.DeleteOptions{}); err != nil && !apierrors.IsNotFound(err) {
		return err
	}
	return nil
}

func (k *MongosKeeper) destroy() error {
	objs := k.podInf.GetIndexer().List()
	for _, obj := range objs {
		pod := obj.(*v1.Pod)
		if err := k.client.Core().Pods(pod.Namespace).Delete(pod.Name, &api.DeleteOptions{}); err != nil && !apierrors.IsNotFound(err) {
			return fmt.Errorf("destroy mongos failed: %v", err)
		}
	}
	objs = k.svcInf.GetIndexer().List()
	for _, obj := range objs {
		svc := obj.(*v1.Service)
		if err := k.client.Core().Pods(svc.Namespace).Delete(svc.Name, &api.DeleteOptions{}); err != nil && !apierrors.IsNotFound(err) {
			return fmt.Errorf("destroy mongos svc failed: %v", err)
		}
	}
	return nil
}

// Run mongos keeper
func (k *MongosKeeper) Run() {
	go k.syncStateworker()
	go k.syncSvcWorker()
	go k.podInf.Run(k.stopCh)
	go k.svcInf.Run(k.stopCh)
	k.enqueue("init")
	<-k.stopCh
}

// Update mongos keeper
func (k *MongosKeeper) Update() error {
	glog.Infof("update of mongos keeper is not implemented")
	return nil
}

func (k *MongosKeeper) isDeletionFinished() bool {
	if !k.isInSync() {
		return false
	}
	return len(k.podInf.GetIndexer().List()) == 0 && len(k.svcInf.GetIndexer().List()) == 0
}

// Stop mongos keeper
func (k *MongosKeeper) Stop() error {
	k.stopLock.Lock()
	defer k.stopLock.Unlock()

	k.desc.Status.State = DELETING
	// tell keeper to destory cluster
	if err := k.destroy(); err != nil {
		return err
	}
	for i := 0; i < 10; i++ {
		if k.isDeletionFinished() {
			break
		}
		if i >= 9 {
			return fmt.Errorf("mongo keeper haven't finish deletion")
		}
		time.Sleep(time.Second)
	}

	if !k.shutdown {
		k.shutdown = true
		k.queue.ShutDown()
		k.svcSyncQueue.ShutDown()
		close(k.stopCh)
		k.desc.Status.State = DELETED
		return nil
	}
	return fmt.Errorf("shutdown already in progress")

}

// GetStatus returns mongos keeper status
func (k *MongosKeeper) GetStatus() MongosStatus {
	return k.desc.Status
}
