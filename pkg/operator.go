package pkg

import (
	"fmt"
	"reflect"
	"sync"
	"time"

	"bitbucket.org/_yakumo/mongo-operator/pkg/queue"
	"github.com/golang/glog"

	"k8s.io/client-go/1.5/pkg/api"
	apierrors "k8s.io/client-go/1.5/pkg/api/errors"
	utilruntime "k8s.io/client-go/1.5/pkg/util/runtime"
	"k8s.io/client-go/1.5/tools/cache"
)

var (
	resyncPeriod = 5 * time.Minute
)

// Operator defination
type Operator struct {
	stopCh   chan struct{}
	stopLock sync.Mutex
	shutdown bool
	client   *K8sClient
	mongoInf cache.SharedIndexInformer
	keepers  map[string]MongoKeeper
	queue    *queue.Queue
}

// NewOperator return an operator instance
func NewOperator(inCluster bool) (*Operator, error) {
	if err := fetchClusterDomain(); inCluster && err != nil {
		return nil, err
	}
	cfg, err := NewClientConfig(inCluster)
	if err != nil {
		return nil, err
	}
	client, err := NewK8sClient(cfg)
	if err != nil {
		return nil, err
	}
	if err != nil {
		return nil, err
	}
	o := Operator{
		client:  client,
		queue:   queue.New(),
		keepers: map[string]MongoKeeper{},
		stopCh:  make(chan struct{}),
	}
	o.mongoInf = cache.NewSharedIndexInformer(
		NewMongoListWatch(client),
		&Mongo{}, resyncPeriod, cache.Indexers{},
	)

	o.mongoInf.AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc:    o.handleAddMongo,
		DeleteFunc: o.handleDeleteMongo,
		UpdateFunc: o.handleUpdateMongo,
	})
	return &o, nil
}

func (o *Operator) handleAddMongo(obj interface{}) {
	key, ok := o.keyFunc(obj)
	if !ok {
		return
	}
	glog.Infof("mongo cluster added: %s", key)
	o.enqueue(key)
}

func (o *Operator) handleDeleteMongo(obj interface{}) {
	key, ok := o.keyFunc(obj)
	if !ok {
		return
	}
	glog.Infof("mongo cluster deleted: %s", key)
	o.enqueue(key)
}

func (o *Operator) handleUpdateMongo(old, cur interface{}) {
	if reflect.DeepEqual(old, cur) {
		return
	}
	key, ok := o.keyFunc(cur)
	if !ok {
		return
	}
	glog.Infof("mongo cluster updated: %s", key)
	o.enqueue(key)
}

// enqueue adds a key to the queue. If obj is a key already it gets added directly.
// Otherwise, the key is extracted via keyFunc.
func (o *Operator) enqueue(obj interface{}) {
	if obj == nil {
		return
	}
	key, ok := obj.(string)
	if !ok {
		key, ok = o.keyFunc(obj)
		if !ok {
			return
		}
	}
	o.queue.Add(key)
}

func (o *Operator) keyFunc(obj interface{}) (string, bool) {
	k, err := cache.DeletionHandlingMetaNamespaceKeyFunc(obj)
	if err != nil {
		glog.Errorf("create key failed: %v", err)
		return k, false
	}
	return k, true
}

func (o *Operator) createTPR() error {
	if _, err := o.client.Extensions().ThirdPartyResources().Create(NewMongoThirdPartyResoruce()); err != nil && !apierrors.IsAlreadyExists(err) {
		return err
	}
	return nil
}

func (o *Operator) deleteTPR() error {
	if err := o.client.Extensions().ThirdPartyResources().Delete("mongo.infra.caicloud.io", &api.DeleteOptions{}); err != nil {
		return err
	}
	return nil
}

func (o *Operator) worker() {
	for {
		key, quit := o.queue.Get()
		if quit {
			return
		}
		if err := o.sync(key.(string)); err != nil {
			utilruntime.HandleError(fmt.Errorf("reconciliation failed, re-enqueueing: %s", err))
			// We only mark the item as done after waiting. In the meantime
			// other items can be processed but the same item won't be processed again.
			// This is a trivial form of rate-limiting that is sufficient for our throughput
			// and latency expectations.
			go func() {
				time.Sleep(3 * time.Second)
				o.queue.Done(key)
				o.queue.Add(key)
			}()
			continue
		}
		o.queue.Done(key)
	}
}

func (o *Operator) sync(key string) error {
	obj, exists, err := o.mongoInf.GetIndexer().GetByKey(key)
	if err != nil {
		return err
	}
	if !exists {
		if k, ok := o.keepers[key]; ok {
			err := k.Stop()
			if err != nil {
				return err
			}
			if o.keepers[key].GetStatus().State != DELETED {
				return fmt.Errorf("mongo keeper %s is not marked DELETED", key)
			}
		}
		delete(o.keepers, key)
		glog.Infof("mongo keeper %s successfully stopped", key)
		return nil
	}
	mongoDesc := obj.(*Mongo)
	if _, ok := o.keepers[key]; !ok {
		if mongoDesc.Spec.Sharding == nil {
			o.keepers[key] = NewReplicaSetKeeper(o.client, mongoDesc, nil)
		} else {
			o.keepers[key] = NewShardingKeeper(o.client, mongoDesc)
		}
		go o.keepers[key].Run()
		return nil
	}
	return o.keepers[key].Update(mongoDesc)
}

// Run the operator
func (o *Operator) Run() error {
	v, err := o.client.Discovery().ServerVersion()
	if err != nil {
		return fmt.Errorf("communicating with server failed: %s", err)
	}
	glog.Infof("connection established, cluster-version: %v", v)
	if err := o.createTPR(); err != nil {
		return err
	}
	if err := WaitForTPRReady(o.client.RESTClient, TPRGroup, TPRVersion, TPRMongoKind); err != nil {
		return err
	}
	glog.Infof("TPR is ready")

	go o.worker()
	go o.mongoInf.Run(o.stopCh)

	<-o.stopCh
	return nil
}

func (o *Operator) waitDeletingKeepers() {
	for key, keeper := range o.keepers {
		for {
			state := keeper.GetStatus().State
			switch state {
			case DELETING:
				glog.Warningf("keeper of %s is %s, wait here", key, state)
				time.Sleep(time.Second)
				continue
			case RECOVERING:
				glog.Warningf("keeper of %s is %s, wait here", key, state)
				time.Sleep(time.Second)
				continue
			default:
				glog.Warningf("keeper of %s is %s", key, state)
			}
			break
		}
	}
}

// Stop the operator
func (o *Operator) Stop() error {
	o.stopLock.Lock()
	defer o.stopLock.Unlock()

	if !o.shutdown {
		o.waitDeletingKeepers()
		glog.Infof("delete TPR")
		if err := o.deleteTPR(); err != nil {
			return err
		}
		o.queue.ShutDown()
		close(o.stopCh)
		o.shutdown = true
		return nil
	}

	return fmt.Errorf("shutdown already in progress")
}
