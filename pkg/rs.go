package pkg

import (
	"fmt"
	"reflect"
	"strings"
	"sync"
	"time"

	"bitbucket.org/_yakumo/mongo-operator/pkg/queue"
	"github.com/golang/glog"
	"k8s.io/client-go/1.5/pkg/api"
	apierrors "k8s.io/client-go/1.5/pkg/api/errors"
	"k8s.io/client-go/1.5/pkg/api/v1"
	"k8s.io/client-go/1.5/pkg/labels"
	"k8s.io/client-go/1.5/tools/cache"
)

var _ MongoKeeper = (*ReplicaSetKeeper)(nil)

type updateDescCallback func(desc *Mongo) error

// ReplicaSetKeeper holds one mongo replicaset
type ReplicaSetKeeper struct {
	kind         string
	client       *K8sClient
	desc         *Mongo
	stopCh       chan struct{}
	stopLock     sync.Mutex
	shutdown     bool
	podInf       cache.SharedIndexInformer
	svcInf       cache.SharedIndexInformer
	queue        *queue.Queue
	svcSyncQueue *queue.Queue
	sm           *stateMachine
	callback     updateDescCallback
}

// NewReplicaSetKeeper returns replicaset keeper
// callback is for doing something when the desc is updated by keeper self
func NewReplicaSetKeeper(client *K8sClient, desc *Mongo, callback updateDescCallback) MongoKeeper {
	keeper := ReplicaSetKeeper{
		kind:         "replicaset",
		client:       client,
		desc:         desc,
		stopCh:       make(chan struct{}),
		queue:        queue.New(),
		svcSyncQueue: queue.New(),
		sm:           &stateMachine{state: INITIALIZING, replicas: desc.Spec.Replicas},
		callback:     callback,
	}
	if keeper.callback == nil {
		keeper.callback = func(desc *Mongo) error {
			desc, err := keeper.client.UpdateMongo(desc.Namespace, desc.Name, desc)
			if err != nil {
				return fmt.Errorf("update mongo state failed: %v", err)
			}
			keeper.desc = desc
			return nil
		}
	}
	keeper.podInf = cache.NewSharedIndexInformer(
		NewListWatchFromClient(keeper.client.Core().GetRESTClient(),
			desc.Namespace,
			"pods",
			labels.SelectorFromSet(map[string]string{mongoReplSetLabel: fmt.Sprintf("%s-mongo", desc.Name)})),
		&v1.Pod{}, resyncPeriod, cache.Indexers{},
	)
	keeper.podInf.AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc:    func(obj interface{}) { keeper.enqueue(obj); keeper.svcEnqueue(obj) },
		DeleteFunc: func(obj interface{}) { keeper.enqueue(obj); keeper.svcEnqueue(obj) },
		UpdateFunc: func(old, cur interface{}) {
			if !reflect.DeepEqual(old, cur) {
				keeper.enqueue(cur)
				keeper.svcEnqueue(cur)
			}
		},
	})
	keeper.svcInf = cache.NewSharedIndexInformer(
		NewListWatchFromClient(keeper.client.Core().GetRESTClient(),
			desc.Namespace,
			"services",
			labels.SelectorFromSet(map[string]string{mongoReplSetLabel: fmt.Sprintf("%s-mongo", desc.Name)})),
		&v1.Service{}, resyncPeriod, cache.Indexers{},
	)
	keeper.podInf.AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc:    func(obj interface{}) { keeper.svcEnqueue(obj) },
		DeleteFunc: func(obj interface{}) { keeper.svcEnqueue(obj) },
		UpdateFunc: func(old, cur interface{}) {
			if !reflect.DeepEqual(old, cur) {
				keeper.svcEnqueue(cur)
			}
		},
	})
	return &keeper
}

func (k *ReplicaSetKeeper) keyFunc(obj interface{}) (string, bool) {
	key, err := cache.DeletionHandlingMetaNamespaceKeyFunc(obj)
	if err != nil {
		glog.Errorf("create key failed: %v", err)
		return key, false
	}
	return key, true
}

func (k *ReplicaSetKeeper) enqueue(obj interface{}) {
	if obj == nil {
		return
	}
	key, ok := obj.(string)
	if !ok {
		key, ok = k.keyFunc(obj)
		if !ok {
			return
		}
	}
	k.queue.Add(key)
}

func (k *ReplicaSetKeeper) svcEnqueue(obj interface{}) {
	if obj == nil {
		return
	}
	key, ok := obj.(string)
	if !ok {
		key, ok = k.keyFunc(obj)
		if !ok {
			return
		}
	}
	k.svcSyncQueue.Add(key)
}

func (k *ReplicaSetKeeper) syncStateWorker() {
	for {
		key, quit := k.queue.Get()
		if quit {
			return
		}
		if err := k.syncState(key.(string)); err != nil {
			glog.Errorf("reconciliation failed, re-enqueueing: %s", err)
			// We only mark the item as done after waiting. In the meantime
			// other items can be processed but the same item won't be processed again.
			// This is a trivial form of rate-limiting that is sufficient for our throughput
			// and latency expectations.
			go func() {
				time.Sleep(3 * time.Second)
				k.queue.Done(key)
				k.queue.Add(key)
			}()
			continue
		}
		k.queue.Done(key)
	}
}

func (k *ReplicaSetKeeper) syncSvcWorker() {
	for {
		key, quit := k.svcSyncQueue.Get()
		if quit {
			return
		}
		if err := k.syncSvc(key.(string)); err != nil {
			glog.Errorf("reconciliation failed, re-enqueueing: %s", err)
			// We only mark the item as done after waiting. In the meantime
			// other items can be processed but the same item won't be processed again.
			// This is a trivial form of rate-limiting that is sufficient for our throughput
			// and latency expectations.
			go func() {
				time.Sleep(3 * time.Second)
				k.svcSyncQueue.Done(key)
				k.svcSyncQueue.Add(key)
			}()
			continue
		}
		k.svcSyncQueue.Done(key)
	}
}

type stateMachine struct {
	sync.Mutex
	replicas int
	state    string
}

type event struct {
	Primary   int
	Secondary int
	Arbiter   int
	Other     int
	SetDelete bool
}

// States of cluster
const (
	INITIALIZING string = "Initializing"
	CREATING     string = "Creating"
	RUNNING      string = "Running"
	RECOVERING   string = "Recovering"
	DELETING     string = "Deleting"
	DELETED      string = "Deleted"
	BROKEN       string = "Broken"
)

// Internal state for transit
const (
	RECOVERABLE   string = "RECOVERABLE"
	QUALIFIED     string = "QUALIFIED"
	ALLABSENT     string = "ALLABSENT"
	UNRECOVERABLE string = "UNRECOVERABLE"
)

func (sm *stateMachine) to(state string) {
	before := sm.state
	sm.state = state
	glog.Infof("state transit from %s to %s", before, sm.state)
}

// Errors
var (
	ErrAlreadyDeleted = fmt.Errorf("the cluster is DELETED, impossible to transit status")
)

func calTotalInstancesAndArbiterExistence(replicas int) (int, bool) {
	if replicas%2 == 0 {
		return replicas + 1, true
	}
	return replicas, false
}

func calTransitEvent(pri, sec, arb int, replicas int) string {
	total, _ := calTotalInstancesAndArbiterExistence(replicas)
	if pri+sec+arb == 0 {
		return ALLABSENT
	} else if pri+sec+arb == total {
		return QUALIFIED
	} else if pri+sec+arb > total/2 && pri+sec <= replicas {
		return RECOVERABLE
	}
	return UNRECOVERABLE
}

func calRoleToRecovery(pri, sec, arb int, replicas int) (string, error) {
	switch calTransitEvent(pri, sec, arb, replicas) {
	case RECOVERABLE:
		if pri+sec == replicas {
			return ARBITER, nil
		}
		return SECONDARY, nil
	default:
		return "", fmt.Errorf("it's not the time to recover")
	}
}

func (sm *stateMachine) Transit(e *event) error {
	sm.Lock()
	defer sm.Unlock()
	if sm.state != DELETED && e.SetDelete {
		sm.to(DELETING)
		return nil
	}
	trans := calTransitEvent(e.Primary, e.Secondary, e.Arbiter, sm.replicas)
	switch sm.state {
	case INITIALIZING:
		switch trans {
		case ALLABSENT:
			sm.to(CREATING)
		case QUALIFIED:
			sm.to(RUNNING)
		case RECOVERABLE:
			sm.to(RECOVERING)
		default:
			sm.to(BROKEN)
		}
		return nil
	case RUNNING:
		switch trans {
		case QUALIFIED:
			sm.to(RUNNING)
		case RECOVERABLE:
			sm.to(RECOVERING)
		default:
			sm.to(BROKEN)
		}
		return nil
	case CREATING:
		switch trans {
		case QUALIFIED:
			sm.to(RUNNING)
		// strict rule here, when creating, if anything is wrong, set BROKEN
		default:
			sm.to(BROKEN)
		}
		return nil
	case RECOVERING:
		switch trans {
		// recoverd
		case QUALIFIED:
			sm.to(RUNNING)
		// two of them alive, recovering?
		case RECOVERABLE:
			sm.to(RECOVERING)
		default:
			sm.to(BROKEN)
		}
		return nil
	case DELETING:
		switch trans {
		case ALLABSENT:
			sm.to(DELETED)
		default:
			sm.to(DELETING)
		}
		return nil
	case BROKEN:
		return nil
	case DELETED:
		return ErrAlreadyDeleted
	default:
		return fmt.Errorf("no transit logic performed")
	}
}

func (sm *stateMachine) State() string {
	sm.Lock()
	defer sm.Unlock()
	return sm.state
}

func (k *ReplicaSetKeeper) determineCurrentState() *event {
	objs := k.podInf.GetIndexer().List()
	total, _ := calTotalInstancesAndArbiterExistence(k.desc.Spec.Replicas)
	if len(objs) > total {
		glog.Errorf("we have %d running mongo instance, more than %d, I cannot handle this situation, so return abnormal event", len(objs), total)
		return &event{Primary: 0, Secondary: 0, Arbiter: 0, Other: 0}
	}
	pri, sec, arb, other := 0, 0, 0, 0
	for _, obj := range objs {
		pod := obj.(*v1.Pod)
		resp, err := mongoShellReplSetStatus(pod.Status.PodIP)
		if err != nil {
			glog.Errorf("fetch replica set status failed: %v", err)
			continue
		}
		for _, member := range resp.Members {
			if member.Self && member.Health == 1 {
				switch member.StateStr {
				case PRIMARY:
					pri++
					k.updatePrimary(pod.Name)
				case SECONDARY:
					sec++
				case ARBITER:
					arb++
				default:
					glog.Infof("unwanted state: %s", member.StateStr)
					other++
				}
			}
		}
	}
	return &event{
		Primary:   pri,
		Secondary: sec,
		Arbiter:   arb,
		Other:     other,
	}
}

func (k *ReplicaSetKeeper) syncState(key string) error {
	// what should sync do:
	// we implement the keeper as a state mechine
	// 1. determine/reconcile(should be only happen when operator self restarted unexpectedly) current cluster state
	// 2. check the state change. if nothing happened, loop; if state changed, check validity
	if !k.isInSync() {
		return fmt.Errorf("deferring sync till informer has synced")
	}
	if key == "delete" {
		k.sm.Transit(&event{SetDelete: true})
	} else {
		k.sm.Transit(k.determineCurrentState())
	}
	k.updateState(k.sm.State())
	switch k.sm.State() {
	case CREATING:
		return k.create()
	case RECOVERING:
		return k.recov()
	case BROKEN:
		return k.sendBrokenEvent()
	case DELETING:
		return k.destroy()
	case RUNNING:
		fallthrough
	case DELETED:
		fallthrough
	default:
		return nil
	}
}

func (k *ReplicaSetKeeper) syncSvc(key string) error {
	if !k.isInSync() {
		return fmt.Errorf("deferring sync till informer has synced")
	}
	primaryName := k.desc.Status.Primary
	primarySvcName := fmt.Sprintf("%s-mongo", k.desc.Name)
	for _, obj := range k.podInf.GetIndexer().List() {
		pod := obj.(*v1.Pod)
		svc := makeMongoSvc(pod)
		if _, err := k.client.Core().Services(svc.Namespace).Create(svc); err != nil && !apierrors.IsAlreadyExists(err) {
			return err
		}
		if pod.Name == primaryName {
			if svc, err := k.client.Core().Services(k.desc.Namespace).Get(primarySvcName); err != nil {
				if apierrors.IsNotFound(err) {
					svc := makeMongoPrimarySvc(primarySvcName, pod)
					if _, err := k.client.Core().Services(svc.Namespace).Create(svc); err != nil {
						return err
					}
				} else {
					return err
				}
			} else {
				svc.Spec.Selector = map[string]string{headlessSvcLabel: primaryName}
				if _, err := k.client.Core().Services(svc.Namespace).Update(svc); err != nil {
					return err
				}
			}
		}
	}
	for _, obj := range k.svcInf.GetIndexer().List() {
		svc := obj.(*v1.Service)
		valid := false
		for _, obj2 := range k.podInf.GetIndexer().List() {
			pod := obj2.(*v1.Pod)
			if svc.Name == primarySvcName || svc.Name == pod.Name {
				valid = true
			}
		}
		if !valid {
			if err := k.client.Core().Services(svc.Namespace).Delete(svc.Name, &api.DeleteOptions{}); err != nil && !apierrors.IsNotFound(err) {
				return err
			}
		}
	}
	return nil
}

func (k *ReplicaSetKeeper) isDeletionFinished() bool {
	if !k.isInSync() {
		return false
	}
	return k.sm.State() == DELETED && len(k.podInf.GetIndexer().List()) == 0 && len(k.svcInf.GetIndexer().List()) == 0
}

func (k *ReplicaSetKeeper) isInSync() bool {
	return k.podInf.HasSynced() && k.svcInf.HasSynced()
}

// Run mongo keeper
func (k *ReplicaSetKeeper) Run() {
	go k.syncStateWorker()
	go k.syncSvcWorker()
	go k.podInf.Run(k.stopCh)
	go k.svcInf.Run(k.stopCh)
	k.enqueue("init")
	<-k.stopCh
}

// Update keeper's desc
func (k *ReplicaSetKeeper) Update(desc *Mongo) error {
	if !k.isInSync() {
		return fmt.Errorf("deferring update till informer has synced")
	}
	if k.desc.Status.State != RUNNING {
		if k.desc.Status.State == DELETING || k.desc.Status.State == DELETED {
			if err := k.client.DeleteMongo(desc.Namespace, desc.Name, &api.DeleteOptions{}); err != nil {
				return fmt.Errorf("try deleting mongo description failed")
			}
			return fmt.Errorf("this cluster is being deleting")
		}
		return fmt.Errorf("updating cluster can only be performed when cluster is RUNNING")
	}
	// TODO: do more check here, perform different updating logic
	// together with the state machine!!!
	if desc.Spec != k.desc.Spec {
		glog.Errorf("update spec is not supported here, reset")
		desc.Spec = k.desc.Spec
		desc.Status = k.desc.Status
		desc, err := k.client.UpdateMongo(desc.Namespace, desc.Name, desc)
		if err != nil {
			return fmt.Errorf("update mongo state failed: %v", err)
		}
		k.desc = desc
		k.svcEnqueue("desc-update")
		// Note: here the update is triggered by outside, so no need to do callback here!
	}
	return nil
}

// Stop mongo keeper
func (k *ReplicaSetKeeper) Stop() error {
	k.stopLock.Lock()
	defer k.stopLock.Unlock()

	if !k.shutdown {
		// tell keeper to destory cluster
		k.enqueue("delete")
		for i := 0; i < 10; i++ {
			if k.isDeletionFinished() {
				break
			}
			if i >= 9 {
				return fmt.Errorf("mongo keeper haven't finish deletion")
			}
			time.Sleep(time.Second)
		}

		k.queue.ShutDown()
		k.svcSyncQueue.ShutDown()
		close(k.stopCh)
		glog.Infof("rs keeper successfully stopped")
		k.shutdown = true
		return nil
	}
	return fmt.Errorf("shutdown already in progress")
}

func (k *ReplicaSetKeeper) create() error {
	instances := []*v1.Pod{}
	if k.desc.Spec.Replicas < 2 {
		return fmt.Errorf("a mongo cluster cannot have number of replicas smaller than 2")
	}
	total, isArbiterNeeded := calTotalInstancesAndArbiterExistence(k.desc.Spec.Replicas)
	for i := 0; i < total; i++ {
		glog.Infof("create mongo instance of id: %d", i)
		pod := makeMongoPod(k.desc.Namespace, k.desc.Name, k.desc.Spec.Image, k.desc.Spec.Version, k.desc.Spec.Role)
		pod, err := k.client.Core().Pods(k.desc.Namespace).Create(pod)
		if err != nil {
			return err
		}
		pod, err = waitForMongoPodRunning(k.client, pod.Namespace, pod.Name)
		if err != nil {
			return err
		}
		instances = append(instances, pod)
	}
	hosts := []string{}
	for _, instance := range instances {
		hosts = append(hosts, fmt.Sprintf("%s:%d", instance.Name, mongoDefaultPort))
	}
	err := mongoShellReplSetInit(
		instances[0].Status.PodIP,
		fmt.Sprintf("%s-mongo", k.desc.Name),
		k.desc.Spec.Role == "configsvr",
		isArbiterNeeded,
		hosts,
	)
	if err != nil {
		return err
	}
	if err := waitForMongoToBe(instances[0], instances[0], PRIMARY); err != nil {
		return err
	}
	return nil
}

func (k *ReplicaSetKeeper) recov() error {
	// TODO: refactor!!!
	glog.Infof("recover cluster here")
	total, _ := calTotalInstancesAndArbiterExistence(k.desc.Spec.Replicas)
	var primaryPod *v1.Pod
	var status *mongoReplSetStatusResp
	for _, obj := range k.podInf.GetIndexer().List() {
		pod := obj.(*v1.Pod)
		resp, err := mongoShellReplSetStatus(pod.Status.PodIP)
		if err != nil {
			continue
		}
		for _, member := range resp.Members {
			if member.Self && member.Health == 1 && member.StateStr == PRIMARY {
				k.updatePrimary(pod.Name)
				primaryPod = pod
				status = resp
				goto rec
			}
		}
	}
	return fmt.Errorf("cannot find primary node currently")

rec:
	e := k.determineCurrentState()
	role, err := calRoleToRecovery(e.Primary, e.Secondary, e.Arbiter, k.desc.Spec.Replicas)
	if err != nil {
		return err
	}
	var memberName string
	var podName string
	rsName := status.Set
	for _, member := range status.Members {
		if member.Health == 0 {
			memberName = member.Name
			podName = strings.Split(member.Name, ":")[0]
			if err := k.client.Core().Pods(k.desc.Namespace).Delete(podName, &api.DeleteOptions{}); err != nil && !apierrors.IsNotFound(err) {
				return fmt.Errorf("clear bad pod failed: %v", err)
			}
		} else if member.StateStr == "STARTUP" || member.StateStr == "STARTUP2" {
			return fmt.Errorf("some instances are starting up, wait here")
		}
	}
	// everytime we only recover one instance
	var new *v1.Pod
	if podName == "" && len(status.Members) == total {
		return fmt.Errorf("cannot find previous pod name, all instance is healthy")
	} else if podName == "" && len(status.Members) < total {
		glog.Warningf("cannot find previous mongo name, generate a new one")
		new = makeMongoPod(k.desc.Namespace, k.desc.Name, k.desc.Spec.Image, k.desc.Spec.Version, k.desc.Spec.Role)
	} else {
		new = makeMongoPodWithFullName(k.desc.Namespace, podName, rsName, k.desc.Spec.Image, k.desc.Spec.Version, k.desc.Spec.Role)
	}
	if _, err := k.client.Core().Pods(new.Namespace).Create(new); err != nil {
		return err
	}
	new, err = waitForMongoPodRunning(k.client, new.Namespace, new.Name)
	if err != nil {
		return err
	}
	if err := mongoShellReplSetRemoveMember(primaryPod.Status.PodIP, memberName); err != nil {
		return err
	}
	if role == SECONDARY {
		err = mongoShellReplSetAddMember(primaryPod.Status.PodIP, new.Name)
	} else if role == ARBITER {
		err = mongoShellReplSetAddArbiter(primaryPod.Status.PodIP, new.Name)
	}
	if err != nil {
		return fmt.Errorf("instance join replica set failed")
	}
	err = waitForMongoToBe(primaryPod, new, role)
	if err != nil {
		if err := k.client.Core().Pods(new.Namespace).Delete(new.Name, &api.DeleteOptions{}); err != nil {
			glog.Errorf("failed to clean pod")
		}
		return fmt.Errorf("wait for mongo %s to be %s failed, remove this pod: %v", new.Name, role, err)
	}
	return nil
}

func (k *ReplicaSetKeeper) sendBrokenEvent() error {
	glog.Errorf("mongo cluster: %s/%s is broken", k.desc.Namespace, k.desc.Name)
	return nil
}

func (k *ReplicaSetKeeper) destroy() error {
	glog.Infof("destroy cluster here")
	if !k.isInSync() {
		return fmt.Errorf("deferring destroy till informer has synced")
	}
	for _, obj := range k.podInf.GetIndexer().List() {
		pod := obj.(*v1.Pod)
		if err := k.client.Core().Pods(pod.Namespace).Delete(pod.Name, &api.DeleteOptions{}); err != nil && !apierrors.IsNotFound(err) {
			return err
		}
	}
	for _, obj := range k.svcInf.GetIndexer().List() {
		svc := obj.(*v1.Service)
		if err := k.client.Core().Services(svc.Namespace).Delete(svc.Name, &api.DeleteOptions{}); err != nil && !apierrors.IsNotFound(err) {
			return err
		}
	}
	return nil
}

func (k *ReplicaSetKeeper) updateState(state string) {
	//TODO: handle state here seems not that good
	if k.desc.Status.State != state {
		k.desc.Status.State = state
		if state != DELETING && state != DELETED {
			if err := k.callback(k.desc); err != nil {
				glog.Errorf("callback failed: %v", err)
			}
		}
	}
}

func (k *ReplicaSetKeeper) updatePrimary(primary string) {
	if k.desc.Status.Primary != primary {
		k.desc.Status.Primary = primary
		if err := k.callback(k.desc); err != nil {
			glog.Errorf("callback failed: %v", err)
		}
		k.svcEnqueue("desc-update")
	}
}

// GetStatus return rs status
func (k *ReplicaSetKeeper) GetStatus() MongoStatus {
	return k.desc.Status
}
