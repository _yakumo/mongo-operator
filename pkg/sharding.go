package pkg

import (
	"fmt"
	"reflect"
	"sync"
	"time"

	"bitbucket.org/_yakumo/mongo-operator/pkg/queue"
	"github.com/golang/glog"
	"k8s.io/client-go/1.5/pkg/api"
	"k8s.io/client-go/1.5/pkg/api/v1"
	"k8s.io/client-go/1.5/tools/cache"
)

var _ MongoKeeper = (*ShardingKeeper)(nil)

// State for sharding
const (
	SCALING string = "Scaling"
)

// ShardingKeeper holds one mongo sharding cluster
type ShardingKeeper struct {
	kind     string
	client   *K8sClient
	desc     *Mongo
	stopCh   chan struct{}
	stopLock sync.Mutex
	shutdown bool
	queue    *queue.Queue
	mongos   *MongosKeeper
	configRS MongoKeeper
	shardsRS map[string]MongoKeeper
	state    string
}

// NewShardingKeeper returns sharding keeper
func NewShardingKeeper(client *K8sClient, desc *Mongo) MongoKeeper {
	desc.Status.State = INITIALIZING
	keeper := ShardingKeeper{
		kind:     "sharding",
		client:   client,
		desc:     desc,
		stopCh:   make(chan struct{}),
		queue:    queue.New(),
		configRS: nil,
		mongos:   nil,
		shardsRS: map[string]MongoKeeper{},
		state:    INITIALIZING,
	}
	return &keeper
}

func (k *ShardingKeeper) keyFunc(obj interface{}) (string, bool) {
	key, err := cache.DeletionHandlingMetaNamespaceKeyFunc(obj)
	if err != nil {
		glog.Errorf("create key failed: %v", err)
		return key, false
	}
	return key, true
}

func (k *ShardingKeeper) enqueue(obj interface{}) {
	if obj == nil {
		return
	}
	key, ok := obj.(string)
	if !ok {
		key, ok = k.keyFunc(obj)
		if !ok {
			return
		}
	}
	k.queue.Add(key)
}

func (k *ShardingKeeper) worker() {
	for {
		key, quit := k.queue.Get()
		if quit {
			return
		}
		if err := k.syncState(key.(string)); err != nil {
			glog.Errorf("reconciliation failed, re-enqueueing: %s", err)
			// We only mark the item as done after waiting. In the meantime
			// other items can be processed but the same item won't be processed again.
			// This is a trivial form of rate-limiting that is sufficient for our throughput
			// and latency expectations.
			go func() {
				time.Sleep(3 * time.Second)
				k.queue.Done(key)
				k.queue.Add(key)
			}()
			continue
		}
		k.queue.Done(key)
	}
}

func (k *ShardingKeeper) getOverallState() string {
	if k.desc.ShardingStatus.CSRS.State == BROKEN {
		return BROKEN
	}
	for _, shard := range k.desc.ShardingStatus.Shards {
		if shard.State == BROKEN {
			return BROKEN
		}
	}
	if k.desc.ShardingStatus.CSRS.State == RECOVERING {
		return RECOVERING
	}
	if k.desc.ShardingStatus.Mongos.State == RECOVERING {
		return RECOVERING
	}
	for _, shard := range k.desc.ShardingStatus.Shards {
		if shard.State == RECOVERING {
			return RECOVERING
		}
	}
	return RUNNING
}

func (k *ShardingKeeper) recov() error {
	err := k.createAndWaitCSRS()
	if err != nil {
		return err
	}
	k.desc.ShardingStatus.CSRS = k.configRS.GetStatus()

	err = k.createAndWaitMongos(fmt.Sprintf("%s-configsvr-mongo/%s:%d", k.desc.Name, k.configRS.GetStatus().Primary, mongoDefaultPort))
	if err != nil {
		return err
	}
	k.desc.ShardingStatus.Mongos = k.mongos.GetStatus()

	err = k.createAndWaitShards()
	if err != nil {
		return err
	}
	k.desc.ShardingStatus.Shards = k.getShardsStatus()
	k.desc.Status.State = k.getOverallState()

	for key, shard := range k.shardsRS {
		if err := mongoShellAddShard(k.mongos.GetStatus().Hosts[0], fmt.Sprintf("%s-mongo/%s", key, shard.GetStatus().Primary)); err != nil {
			return err
		}
	}
	for _, col := range k.desc.Spec.Sharding.Collections {
		err := mongoShellEnableSharding(k.mongos.GetStatus().Hosts[0], col.DB)
		if err != nil {
			return err
		}
		namespace := fmt.Sprintf("%s.%s", col.DB, col.Collection)
		var key string
		if col.Hashed {
			key = fmt.Sprintf("{ %s: \"hashed\" }", col.Key)
		} else {
			key = fmt.Sprintf("{ %s: 1 }", col.Key)
		}
		err = mongoShellShardCollection(k.mongos.GetStatus().Hosts[0], namespace, key, col.Unique)
		if err != nil {
			return err
		}
	}

	desc, err := k.client.UpdateMongo(k.desc.Namespace, k.desc.Name, k.desc)
	if err != nil {
		return err
	}
	k.desc = desc

	return nil

}

func (k *ShardingKeeper) syncState(key string) error {
	switch k.desc.Status.State {
	case INITIALIZING:
		fallthrough
	case SCALING:
		fallthrough
	case RUNNING:
		return k.recov()
	case DELETING:
		return k.destroy()
	case DELETED:
		fallthrough
	default:
		return nil
	}
}

func (k *ShardingKeeper) createAndWaitCSRS() error {
	if k.configRS == nil {
		glog.Infof("creating config server replica set")
		desc := Mongo{
			ObjectMeta: v1.ObjectMeta{
				Name:      fmt.Sprintf("%s-configsvr", k.desc.Name),
				Namespace: k.desc.Namespace,
			},
			Spec: MongoSpec{
				Image:    k.desc.Spec.Image,
				Version:  k.desc.Spec.Version,
				Role:     "configsvr",
				Replicas: 3,
			},
		}
		k.configRS = NewReplicaSetKeeper(k.client, &desc, func(desc *Mongo) error {
			k.enqueue(desc.Name)
			return nil
		})
		go k.configRS.Run()
	}
	for i := 0; i < 10; i++ {
		glog.Infof("wait for config svr ready...")
		if k.configRS.GetStatus().State == RUNNING {
			return nil
		}
		time.Sleep(5 * time.Second)
	}
	return fmt.Errorf("wait for config svr ready timeout")
}

func (k *ShardingKeeper) createAndWaitMongos(configsvr string) error {
	if k.mongos == nil {
		glog.Infof("creating mongos keeper")
		desc := Mongos{
			ObjectMeta: v1.ObjectMeta{
				Name:      k.desc.Name,
				Namespace: k.desc.Namespace,
			},
			Spec: MongosSpec{
				Image:     k.desc.Spec.Image,
				Version:   k.desc.Spec.Version,
				ConfigSvr: configsvr,
				Size:      2,
			},
		}
		k.mongos = NewMongosKeeper(k.client, &desc)
		go k.mongos.Run()
	}
	for i := 0; i < 10; i++ {
		glog.Infof("wait for mongos ready...")
		if k.mongos.GetStatus().State == RUNNING {
			return nil
		}
		time.Sleep(5 * time.Second)
	}
	return fmt.Errorf("waif for mongos ready timeout")
}

func (k *ShardingKeeper) createAndWaitShards() error {
	for i := 0; i < k.desc.Spec.Sharding.Shards; i++ {
		glog.Infof("creating shard server of id: %d", i)
		shardName := fmt.Sprintf("%s-shard%d", k.desc.Name, i)
		if _, ok := k.shardsRS[shardName]; !ok {
			desc := Mongo{
				ObjectMeta: v1.ObjectMeta{
					Name:      shardName,
					Namespace: k.desc.Namespace,
				},
				Spec: MongoSpec{
					Image:    k.desc.Spec.Image,
					Version:  k.desc.Spec.Version,
					Role:     "shardsvr",
					Replicas: 2,
				},
			}
			k.shardsRS[shardName] = NewReplicaSetKeeper(k.client, &desc, func(desc *Mongo) error {
				k.enqueue(desc.Name)
				return nil
			})
			go k.shardsRS[shardName].Run()
		}
	}
	for i := 0; i < 10; i++ {
		glog.Infof("wait for all shards ready...")
		ready := true
		for _, shard := range k.shardsRS {
			if shard.GetStatus().State != RUNNING {
				ready = false
			}
		}
		if ready {
			return nil
		}
		time.Sleep(5 * time.Second)
	}
	return fmt.Errorf("wait for all shards ready timeout")
}

func (k *ShardingKeeper) getShardsStatus() map[string]MongoStatus {
	status := map[string]MongoStatus{}
	for key, shard := range k.shardsRS {
		status[key] = shard.GetStatus()
	}
	return status
}

// Run the sharding keeper
func (k *ShardingKeeper) Run() {
	go k.worker()
	k.enqueue("init")
	<-k.stopCh
}

// Update sharding keeper's config
func (k *ShardingKeeper) Update(desc *Mongo) error {
	if k.desc.Status.State != RUNNING {
		if k.desc.Status.State == DELETING || k.desc.Status.State == DELETED {
			if err := k.client.DeleteMongo(desc.Namespace, desc.Name, &api.DeleteOptions{}); err != nil {
				return fmt.Errorf("try deleting mongo description failed")
			}
			return fmt.Errorf("this cluster is being deleting")
		}
		return fmt.Errorf("updating cluster can only be performed when cluster is RUNNING")
	}
	// TODO: do more check here, perform different updating logic
	// here we only apply the update on Spec.Sharding.Shards
	if !reflect.DeepEqual(k.desc, desc) {
		shards := desc.Spec.Sharding.Shards
		desc.Spec = k.desc.Spec
		desc.Status = k.desc.Status
		desc.ShardingStatus = k.desc.ShardingStatus
		if shards >= k.desc.Spec.Sharding.Shards {
			desc.Spec.Sharding.Shards = shards
			desc.Status.State = SCALING
		} else {
			glog.Errorf("cannot apply shards number smaller than before")
		}
		desc, err := k.client.UpdateMongo(desc.Namespace, desc.Name, desc)
		if err != nil {
			return fmt.Errorf("update mongo state failed: %v", err)
		}
		k.desc = desc
		k.enqueue("update")
	}
	return nil
}

func (k *ShardingKeeper) destroy() error {
	if k.mongos != nil {
		if err := k.mongos.Stop(); err != nil {
			return err
		}
		k.mongos = nil
	}

	if k.configRS != nil {
		if err := k.configRS.Stop(); err != nil {
			return err
		}
		k.configRS = nil
	}

	// TODO: use goroutine to get quicker
	for key, shard := range k.shardsRS {
		if err := shard.Stop(); err != nil {
			return err
		}
		delete(k.shardsRS, key)
	}
	return nil
}

func (k *ShardingKeeper) isDeletionFinished() bool {
	if k.desc.Status.State == DELETING && k.configRS == nil && k.mongos == nil && len(k.shardsRS) == 0 {
		k.desc.Status.State = DELETED
	}
	return k.desc.Status.State == DELETED
}

// Stop the sharding keeper
func (k *ShardingKeeper) Stop() error {
	k.stopLock.Lock()
	defer k.stopLock.Unlock()

	if !k.shutdown {
		k.desc.Status.State = DELETING
		k.enqueue("delete")

		for i := 0; i < 10; i++ {
			if k.isDeletionFinished() {
				break
			}
			if i >= 9 {
				return fmt.Errorf("mongo sharding keeper haven't finish deletion")
			}
			time.Sleep(time.Second)
		}

		k.queue.ShutDown()
		close(k.stopCh)
		glog.Infof("sharding keeper successfully stopped")
		k.desc.Status.State = DELETED
		k.shutdown = true
		return nil
	}
	return fmt.Errorf("shutdown already in progress")
}

// GetStatus return status
func (k *ShardingKeeper) GetStatus() MongoStatus {
	return k.desc.Status
}
