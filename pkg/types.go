package pkg

import (
	"k8s.io/client-go/1.5/pkg/api/unversioned"
	"k8s.io/client-go/1.5/pkg/api/v1"
	extensionsobj "k8s.io/client-go/1.5/pkg/apis/extensions/v1beta1"
)

// TPR consts
const (
	TPRGroup     = "infra.caicloud.io"
	TPRVersion   = "v1"
	TPRMongoKind = "mongos"
	TPRMongo     = "mongo." + TPRGroup
)

// NewMongoThirdPartyResoruce generate ThirdPartyResource of mongo cluster
func NewMongoThirdPartyResoruce() *extensionsobj.ThirdPartyResource {
	return &extensionsobj.ThirdPartyResource{
		ObjectMeta: v1.ObjectMeta{
			Name: TPRMongo,
		},
		Description: "A new resource representing Mongo Cluster",
		Versions: []extensionsobj.APIVersion{
			extensionsobj.APIVersion{Name: TPRVersion},
		},
	}
}

// Mongo describe a mongo cluster
type Mongo struct {
	unversioned.TypeMeta `json:",inline"`
	v1.ObjectMeta        `json:"metadata,omitempty"`
	Spec                 MongoSpec           `json:"spec,omitempty"`
	Status               MongoStatus         `json:"status,omitempty"`
	ShardingStatus       MongoShardingStatus `json:"detailed,omitempty"`
}

// MongoList describe a collection of mongo cluster
type MongoList struct {
	unversioned.TypeMeta `json:",inline"`
	unversioned.ListMeta `json:"metadata,omitempty"`

	Items []*Mongo `json:"items"`
}

// MongoSpec describe a mongo cluster's config
type MongoSpec struct {
	Image    string               `json:"image"`
	Version  string               `json:"version"`
	Sharding *MongoShardingConfig `json:"sharding,omitempty"`
	Replicas int                  `json:"replicas,omitempty"`
	Role     string               `json:"role,omitempty"` // choice: configsvr/shardsvr
}

// MongoShardingConfig describe a sharding config
type MongoShardingConfig struct {
	Shards      int               `json:"shards"`
	Collections []ShardCollection `json:"collections,omitempty"`
}

//ShardCollection describe a sharding collection
type ShardCollection struct {
	DB         string `json:"db"`
	Collection string `json:"collection"`
	Key        string `json:"key"`
	Hashed     bool   `json:"hashed"`
	Unique     bool   `json:"unique"`
}

// MongoStatus describe a mongo cluster's status
type MongoStatus struct {
	State   string `json:"state,omitempty"`
	Primary string `json:"primary,omitempty"`
}

// MongoShardingStatus describe a mongo sharding detailed status
type MongoShardingStatus struct {
	CSRS   MongoStatus            `json:"csrs,omitempty"`
	Mongos MongosStatus           `json:"mongos,omitempty"`
	Shards map[string]MongoStatus `json:"shards,omitempty"`
}

// Mongos describe a mongos proxy
type Mongos struct {
	unversioned.TypeMeta `json:",inline"`
	v1.ObjectMeta        `json:"metadata,omitempty"`
	Spec                 MongosSpec   `json:"spec,omitempty"`
	Status               MongosStatus `json:"status,omitempty"`
}

// MongosSpec describe a mongos proxy's config
type MongosSpec struct {
	Image     string `json:"image"`
	Version   string `json:"version"`
	Size      int    `json:"size"`
	ConfigSvr string `json:"configsvr"`
}

// MongosStatus describe a mongo proxy's status
type MongosStatus struct {
	State string   `json:"state,omitempty"`
	Hosts []string `json:"hosts,omitempty"`
}

// MongoKeeper is a mongo cluster controller
type MongoKeeper interface {
	Run()
	Update(*Mongo) error
	Stop() error
	GetStatus() MongoStatus
}
