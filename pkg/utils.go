package pkg

import (
	"fmt"
	"math/rand"
	"net"
	"net/http"
	"strings"
	"time"

	"github.com/golang/glog"
	"k8s.io/client-go/1.5/pkg/api/errors"
	"k8s.io/client-go/1.5/pkg/api/v1"
	"k8s.io/client-go/1.5/pkg/util/intstr"
	"k8s.io/client-go/1.5/pkg/util/wait"
	"k8s.io/client-go/1.5/rest"
)

var (
	clusterDomain = "cluster.local"
)

const (
	headlessSvcLabel  = "mongo-headless-svc-key"
	mongoReplSetLabel = "mongo-replicaset-key"
	mongosLabel       = "mongos-proxy-key"
	mongoDefaultPort  = 27017
	retryLimit        = 10
)

// States of mongo instances
const (
	ARBITER   string = "ARBITER"
	PRIMARY   string = "PRIMARY"
	SECONDARY string = "SECONDARY"
	STARTUP   string = "STARTUP"
	STARTUP2  string = "STARTUP2"
)

func fetchClusterDomain() error {
	// This func only works running in cluster
	result, err := net.LookupAddr("10.254.0.1")
	if err != nil {
		return err
	}
	splited := strings.Split(result[0], "svc")
	clusterDomain = strings.Trim(splited[len(splited)-1], ".")
	return nil
}

const letterBytes = "0123456789abcdefghijklmnopqrstuvwxyz"

// RandStringBytesRmndr return a random string of n letters
func RandStringBytesRmndr(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Int63()%int64(len(letterBytes))]
	}
	return string(b)
}

func makeMongoPod(namespace, prefix, image, version, role string) *v1.Pod {
	rsName := fmt.Sprintf("%s-mongo", prefix)
	podName := fmt.Sprintf("%s-mongo-%s", prefix, RandStringBytesRmndr(6))
	podNamespace := namespace
	return makeMongoPodWithFullName(podNamespace, podName, rsName, image, version, role)
}

func makeMongoPodWithFullName(podNamespace, podName, rsName, image, version, role string) *v1.Pod {
	args := []string{
		"mongod",
		"--replSet",
		rsName,
		"--dbpath",
		"/data/db",
	}
	if role != "" {
		args = append(args, fmt.Sprintf("--%s", role), "--port", fmt.Sprintf("%d", mongoDefaultPort))
	}
	containers := []v1.Container{
		v1.Container{
			Name:            "mongod",
			Image:           fmt.Sprintf("%s:%s", image, version),
			ImagePullPolicy: v1.PullAlways,
			Ports: []v1.ContainerPort{
				v1.ContainerPort{
					Name:          "mongo",
					ContainerPort: mongoDefaultPort,
				},
			},
			Env: []v1.EnvVar{
				v1.EnvVar{
					Name:  "POD_NAME",
					Value: podName,
				},
				v1.EnvVar{
					Name:  "POD_NAMESPACE",
					Value: podNamespace,
				},
				v1.EnvVar{
					Name:  "CLUSTER_DOMAIN",
					Value: clusterDomain,
				},
			},
			Args: args,
			VolumeMounts: []v1.VolumeMount{
				v1.VolumeMount{
					Name:      "mongo-db",
					MountPath: "/data/db",
				},
			},
		},
	}
	pod := v1.Pod{
		ObjectMeta: v1.ObjectMeta{
			Name:      podName,
			Namespace: podNamespace,
			// add annotation for reverse DNS
			// Annotations: map[string]string{fixDNSAnnotation: "true"},
			Labels: map[string]string{headlessSvcLabel: podName, mongoReplSetLabel: rsName},
		},
		Spec: v1.PodSpec{
			Containers: containers,
			Volumes: []v1.Volume{
				v1.Volume{
					Name: "mongo-db",
					VolumeSource: v1.VolumeSource{
						EmptyDir: &v1.EmptyDirVolumeSource{
							Medium: "",
						},
					},
				},
			},
		},
	}
	return &pod
}

func makeMongosPod(namespace, prefix, configsvr, image, version string) *v1.Pod {
	colName := fmt.Sprintf("%s-mongos", prefix)
	podName := fmt.Sprintf("%s-mongos-%s", prefix, RandStringBytesRmndr(6))
	podNamespace := namespace
	return makeMongosPodWithFullName(podNamespace, podName, colName, configsvr, image, version)
}

func makeMongosPodWithFullName(podNamespace, podName, colName, configsvr, image, version string) *v1.Pod {
	args := []string{
		"mongos",
		"--configdb",
		configsvr,
		"--port",
		fmt.Sprintf("%d", mongoDefaultPort),
	}
	containers := []v1.Container{
		v1.Container{
			Name:            "mongos",
			Image:           fmt.Sprintf("%s:%s", image, version),
			ImagePullPolicy: v1.PullAlways,
			Ports: []v1.ContainerPort{
				v1.ContainerPort{
					Name:          "mongo",
					ContainerPort: mongoDefaultPort,
				},
			},
			Env: []v1.EnvVar{
				v1.EnvVar{
					Name:  "POD_NAME",
					Value: podName,
				},
				v1.EnvVar{
					Name:  "POD_NAMESPACE",
					Value: podNamespace,
				},
				v1.EnvVar{
					Name:  "CLUSTER_DOMAIN",
					Value: clusterDomain,
				},
			},
			Args: args,
		},
	}
	pod := v1.Pod{
		ObjectMeta: v1.ObjectMeta{
			Name:      podName,
			Namespace: podNamespace,
			// add annotation for reverse DNS
			// Annotations: map[string]string{fixDNSAnnotation: "true"},
			Labels: map[string]string{mongosLabel: colName},
		},
		Spec: v1.PodSpec{
			Containers: containers,
		},
	}
	return &pod
}

func makeMongoPrimarySvc(name string, pod *v1.Pod) *v1.Service {
	svc := v1.Service{
		ObjectMeta: v1.ObjectMeta{
			Name:      name,
			Namespace: pod.Namespace,
			Labels:    map[string]string{mongoReplSetLabel: pod.Labels[mongoReplSetLabel]},
		},
		Spec: v1.ServiceSpec{
			Ports:    generateSvcPorts(pod),
			Selector: map[string]string{headlessSvcLabel: pod.Name},
		},
	}
	return &svc
}

func makeMongoSvc(pod *v1.Pod) *v1.Service {
	svc := v1.Service{
		ObjectMeta: v1.ObjectMeta{
			Name:      pod.Name,
			Namespace: pod.Namespace,
			Labels:    map[string]string{mongoReplSetLabel: pod.Labels[mongoReplSetLabel]},
		},
		Spec: v1.ServiceSpec{
			ClusterIP: "None",
			Ports:     generateSvcPorts(pod),
			Selector:  map[string]string{headlessSvcLabel: pod.Name},
		},
	}
	return &svc
}

func makeMongosSvc(pod *v1.Pod) *v1.Service {
	svc := v1.Service{
		ObjectMeta: v1.ObjectMeta{
			Name:      pod.Labels[mongosLabel],
			Namespace: pod.Namespace,
			Labels:    map[string]string{mongosLabel: pod.Labels[mongosLabel]},
		},
		Spec: v1.ServiceSpec{
			Ports:    generateSvcPorts(pod),
			Selector: map[string]string{mongosLabel: pod.Labels[mongosLabel]},
		},
	}
	return &svc
}

func generateSvcPorts(pod *v1.Pod) []v1.ServicePort {
	ports := []v1.ServicePort{}
	for _, c := range pod.Spec.Containers {
		for _, p := range c.Ports {
			port := v1.ServicePort{
				Port: p.ContainerPort,
				TargetPort: intstr.IntOrString{
					Type:   intstr.Int,
					IntVal: p.ContainerPort,
				},
			}
			port.Name = p.Name
			port.Protocol = p.Protocol
			ports = append(ports, port)
		}
	}
	return ports
}

func waitForMongoPodRunning(c *K8sClient, ns, name string) (*v1.Pod, error) {
	// k8s pod status check
	var pod *v1.Pod
	var err error
k8s:
	for i := 0; i < retryLimit; i++ {
		if pod, err = c.Core().Pods(ns).Get(name); err != nil {
			return nil, err
		} else if pod.Status.Phase == v1.PodRunning {
			goto mongo
		}
		time.Sleep(5 * time.Second)
	}
	if pod.Status.Phase == v1.PodPending {
		glog.Warningf("wait for pod running timeout, but the pod status is Pending, so we continue waiting")
		goto k8s
	}
	return nil, fmt.Errorf("wait for pod running timeout")

mongo:
	// mongo shell check
	for i := 0; i < retryLimit; i++ {
		if err := mongoShellDBStats(pod.Status.PodIP); err != nil {
			glog.Warningf("run mongo dbStats failed: %v", err)
			time.Sleep(5 * time.Second)
		} else {
			return pod, nil
		}
	}
	return nil, fmt.Errorf("run mongo dbStats failed")
}

func waitForMongosPodRunning(c *K8sClient, ns, name string) (*v1.Pod, error) {
	// k8s pod status check
	var pod *v1.Pod
	var err error
k8s:
	for i := 0; i < retryLimit; i++ {
		if pod, err = c.Core().Pods(ns).Get(name); err != nil {
			return nil, err
		} else if pod.Status.Phase == v1.PodRunning {
			goto mongos
		}
		time.Sleep(5 * time.Second)
	}
	if pod.Status.Phase == v1.PodPending {
		glog.Warningf("wait for pod running timeout, but the pod status is Pending, so we continue waiting")
		goto k8s
	}
	return nil, fmt.Errorf("wait for pod running timeout")

mongos:
	// mongo shell check
	for i := 0; i < retryLimit; i++ {
		if err := mongoShellPing(pod.Status.PodIP); err != nil {
			glog.Warningf("ping mongos failed: %v", err)
			time.Sleep(5 * time.Second)
		} else {
			return pod, nil
		}
	}
	return nil, fmt.Errorf("ping mongo failed")
}

func waitForMongoToBe(primary, pod *v1.Pod, role string) error {
	host := ""
	for i := 0; i < retryLimit; i++ {
		resp, err := mongoShellReplSetStatus(pod.Status.PodIP)
		if err != nil {
			glog.Warningf("get repl status failed: %v", err)
		} else {
			for _, member := range resp.Members {
				if member.Self && member.Health == 1 && member.StateStr == role {
					host = member.Name
					goto primary
				}
			}
		}
		time.Sleep(5 * time.Second)
	}
primary:
	for i := 0; i < retryLimit; i++ {
		resp, err := mongoShellReplSetStatus(primary.Status.PodIP)
		glog.Infof("%v", resp)
		if err != nil {
			glog.Warningf("get repl status failed: %v", err)
		} else {
			for _, member := range resp.Members {
				if member.Name == host && member.Health == 1 && member.StateStr == role {
					return nil
				}
			}
		}
		time.Sleep(5 * time.Second)
	}
	return fmt.Errorf("wait for mongo to be %s timeout", role)
}

// WaitForTPRReady waits for a third party resource to be available
// for use.
func WaitForTPRReady(restClient *rest.RESTClient, tprGroup, tprVersion, tprName string) error {
	return wait.Poll(3*time.Second, 30*time.Second, func() (bool, error) {
		res := restClient.Get().AbsPath("apis", tprGroup, tprVersion, tprName).Do()
		err := res.Error()
		if err != nil {
			// RESTClient returns *errors.StatusError for any status codes < 200 or > 206
			// and http.Client.Do errors are returned directly.
			if se, ok := err.(*errors.StatusError); ok {
				if se.Status().Code == http.StatusNotFound {
					return false, nil
				}
			}
			return false, err
		}

		var statusCode int
		res.StatusCode(&statusCode)
		if statusCode != http.StatusOK {
			return false, fmt.Errorf("invalid status code: %d", statusCode)
		}

		return true, nil
	})
}
